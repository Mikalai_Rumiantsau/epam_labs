﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using MyCompany.Resale.BusinessLogic.Data;
using System;

namespace XUnit.Tests.Test
{
    public class ReadFixture : IDisposable
    {
        public SqliteConnection Connection { get; private set; }
        public DbContextOptions<EFCDbContext> Options { get; private set; }

        /// <summary>
        /// Configure connection and init db by tests data.
        /// </summary>
        public ReadFixture()
        {
            Connection = new SqliteConnection("DataSource=:memory:");
            Connection.Open();
            Options = new DbContextOptionsBuilder<EFCDbContext>()
                .UseSqlite(Connection)
                .Options;

            // Create the schema in the database
            using (var context = new EFCDbContext(Options))
            {
                context.Database.EnsureCreated();
            }

            using (var context = new EFCDbContext(Options))
            {
                EFCDbInitializer.Initialize(context);
            }
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}
