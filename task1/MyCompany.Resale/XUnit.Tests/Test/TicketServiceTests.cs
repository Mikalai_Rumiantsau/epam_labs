﻿using MyCompany.Resale.BusinessLogic.Data;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Provider;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace XUnit.Tests.Test
{
    [Collection("DataBaseForReadFixtureCollection")]
    public class TicketServiceTests : IClassFixture<EmptyFixture>
    {
        private ITicketRepository _ticketRepository;
        private IOrderRepository _orderRepository;
        private ITicketService _ticketService;

        private readonly EmptyFixture _emptyFixture;
        private readonly ReadFixture _dbFixture;

        public TicketServiceTests(ReadFixture dbFixture, EmptyFixture emptyFixture)
        {
            _dbFixture = dbFixture;
            _emptyFixture = emptyFixture;
        }

        [Fact]
        public void AddNewTicket_ActualTicket_ActualTicketInDb()
        {
            var expEvent = new Event
            {
                Id = 1,
                Name = "Vitaliy Sychev",
                DateStart = new DateTime(2017, 1, 17),
                DateEnd = new DateTime(2017, 1, 17),
                Banner = "/images/vitaliy-sychev.jpg",
                Description =
                    "Will take place within the framework of the action “Tree of good deeds“, which runs from December 14 to January 15, 2017, at CMU. A portion of the proceeds from tickets sold will be credited to the needy sick children.",
                Status = EventStatus.Activ,
                Venue = null
            };
            var expTicket = new Ticket
            {
                Event = expEvent,
                Number = 2,
                Price = 55,
                Seller = null,
                Status = TicketStatus.WaitingConfirmation
            };

            using (var context = new EFCDbContext(_emptyFixture.Options))
            {
                using (var tr = context.Database.BeginTransaction())
                {
                    _ticketRepository = new TicketRepository(context);
                    _orderRepository = new OrderRepository(context);

                    _ticketService = new TicketService(_ticketRepository, _orderRepository);

                    _ticketService.AddNewTicket(expTicket);

                    var actualTicket = _ticketRepository.Tickets.Value.FirstOrDefault();

                    Assert.Equal(expTicket, actualTicket);

                    tr.Rollback();
                }
            }
        }

        [Fact]
        public void GetTicketByStatus_ActualTicket_ActualTicketInDb()
        {
            var expEvent = new Event
            {
                Id = 1,
                Name = "Vitaliy Sychev",
                DateStart = new DateTime(2017, 1, 17),
                DateEnd = new DateTime(2017, 1, 17),
                Banner = "/images/vitaliy-sychev.jpg",
                Description =
                     "Will take place within the framework of the action “Tree of good deeds“, which runs from December 14 to January 15, 2017, at CMU. A portion of the proceeds from tickets sold will be credited to the needy sick children.",
                Status = EventStatus.Activ,
                Venue = null
            };
            var ticket1 = new Ticket
            {
                Event = expEvent,
                Number = 2,
                Price = 15,
                Seller = new User { Id = 1 },
                Status = TicketStatus.WaitingConfirmation
            };
            var ticket2 = new Ticket
            {
                Event = expEvent,
                Number = 3,
                Price = 40,
                Seller = new User { Id = 2 },
                Status = TicketStatus.WaitingConfirmation
            };

            using (var context = new EFCDbContext(_emptyFixture.Options))
            {
                using (var tr = context.Database.BeginTransaction())
                {
                    _ticketRepository = new TicketRepository(context);
                    _orderRepository = new OrderRepository(context);

                    _ticketService = new TicketService(_ticketRepository, _orderRepository);

                    _ticketService.AddNewTicket(ticket1);
                    _ticketService.AddNewTicket(ticket2);

                    var actualTickets = _ticketService.GetTicketsByStatus(TicketStatus.WaitingConfirmation, 1).Value;
                    var expTickets = new List<Ticket>()
                    {
                        ticket1
                    };

                    Assert.Equal(expTickets, actualTickets);

                    tr.Rollback();
                }
            }
        }
    }
}
