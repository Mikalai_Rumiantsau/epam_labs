﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using MyCompany.Resale.BusinessLogic.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XUnit.Tests.Test
{
    public class EmptyFixture : IDisposable
    {
        public SqliteConnection Connection { get; private set; }
        public DbContextOptions<EFCDbContext> Options { get; private set; }

        /// <summary>
        /// Configure connection and init empty db.
        /// </summary>
        public EmptyFixture()
        {
            Connection = new SqliteConnection("DataSource=:memory:");

            Connection.Open();

            Options = new DbContextOptionsBuilder<EFCDbContext>()
                .UseSqlite(Connection)
                .Options;

            // Create the schema in the database
            using (var context = new EFCDbContext(Options))
            {
                context.Database.EnsureCreated();
            }
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}
