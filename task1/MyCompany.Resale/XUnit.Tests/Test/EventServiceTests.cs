﻿using MyCompany.Resale.BusinessLogic.Data;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Provider;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace XUnit.Tests.Test
{
    [Collection("DataBaseForReadFixtureCollection")]
    public class Tests : IClassFixture<EmptyFixture>
    {
        private CityRepository _cityRepository;
        private IEventRepository _eventRepository;
        private VenueRepository _venueRepository;

        private IEventService _eventService;


        private readonly EmptyFixture _emptyFixture;
        private readonly ReadFixture _dbFixture;

        public Tests(ReadFixture dbFixture, EmptyFixture emptyFixture)
        {
            _dbFixture = dbFixture;
            _emptyFixture = emptyFixture;
        }

        [Fact]
        public void GetaAllEvents_ActualEventsCount()
        {
            using (var context = new EFCDbContext(_dbFixture.Options))
            {
                _cityRepository = new CityRepository(context);
                _eventRepository = new EventRepository(context);
                _venueRepository = new VenueRepository(context);

                _eventService = new EventService(_eventRepository, _cityRepository, _venueRepository);

                var actualCitiesCount = _eventService.GetAllCity().Value.Count;

                Assert.Equal(actualCitiesCount, 6);
            }
        }

        [Fact]
        public void GetEventById_ActualEvent()
        {
            using (var context = new EFCDbContext(_dbFixture.Options))
            {
                using (var tr = context.Database.BeginTransaction())
                {
                    _cityRepository = new CityRepository(context);
                    _eventRepository = new EventRepository(context);
                    _venueRepository = new VenueRepository(context);

                    _eventService = new EventService(_eventRepository, _cityRepository, _venueRepository);

                    var actualVenu = _eventService.GetVenueById(1).Value;

                    Assert.Equal(actualVenu.Id, 1);

                    tr.Rollback();
                }
            }
        }

        [Fact]
        public void AddNewEvent_ActualIvent_ActualEventInDb()
        {
            var expEvent = new Event
            {
                Id = 1,
                Name = "Vitaliy Sychev",
                DateStart = new DateTime(2017, 1, 17),
                DateEnd = new DateTime(2017, 1, 17),
                Banner = "/images/vitaliy-sychev.jpg",
                Description =
                       "Will take place within the framework of the action “Tree of good deeds“, which runs from December 14 to January 15, 2017, at CMU. A portion of the proceeds from tickets sold will be credited to the needy sick children.",
                Status = EventStatus.Activ,
                Venue = null
            };
            using (var context = new EFCDbContext(_emptyFixture.Options))
            {
                using (var tr = context.Database.BeginTransaction())
                {
                    _cityRepository = new CityRepository(context);
                    _eventRepository = new EventRepository(context);
                    _venueRepository = new VenueRepository(context);

                    _eventService = new EventService(_eventRepository, _cityRepository, _venueRepository);

                    _eventService.AddNewEvent(expEvent);

                    var actualEvent = _eventRepository.Events.Value.FirstOrDefault();

                    Assert.Equal(expEvent, actualEvent);

                    tr.Rollback();
                }

            }
        }
    }
}
