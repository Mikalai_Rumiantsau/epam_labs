﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using System;
using MyCompany.Resale.BusinessLogic.Data;

namespace MyCompany.Resale.Presentation
{
    public class AppStart : IStartupFilter
    {
        private readonly IApplicationLifetime _appLifetime;
        private readonly EFCDbContext _context;

        public AppStart(IApplicationLifetime appLifetime, EFCDbContext context)
        {
            _appLifetime = appLifetime;
            _context = context;
        }

        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next) => app =>

                _appLifetime.ApplicationStarted.Register(() =>
                {
                    EFCDbInitializer.Initialize(_context);
                    next(app);
                });

    };
}
