﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MyCompany.Resale.Presentation.Localization
{
    public class RouteCultureProvider : IRequestCultureProvider
    {
        private static RequestCulture requestCulture;
        public Dictionary<string, string> AvaibleCultures = new Dictionary<string, string>();

        public RouteCultureProvider(RequestCulture requestCulture)
        {
            RouteCultureProvider.requestCulture = requestCulture;

            AvaibleCultures.Add("english", "en");
            AvaibleCultures.Add("russian", "ru");
            AvaibleCultures.Add("belarussian", "be");
        }

        public static string DefaultCulture
        {
            get
            {
                return RouteCultureProvider.requestCulture.Culture.TwoLetterISOLanguageName;
            }
        }

        public Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            PathString url = httpContext.Request.Path;

            if (url.ToString().Length <= 1)
            {
                return Task.FromResult<ProviderCultureResult>(
                    new ProviderCultureResult(
                        requestCulture.Culture.TwoLetterISOLanguageName,
                        requestCulture.UICulture.TwoLetterISOLanguageName));
            }

            string culture = httpContext.Request.Path.Value.Split('/').GetValue(1).ToString();

            if (!Regex.IsMatch(culture, @"^[a-z]{2}(-[A-Z]{2})*$"))
            {
                return Task.FromResult<ProviderCultureResult>(
                    new ProviderCultureResult(
                        requestCulture.Culture.TwoLetterISOLanguageName,
                        requestCulture.UICulture.TwoLetterISOLanguageName));
            }

            return Task.FromResult<ProviderCultureResult>(
                new ProviderCultureResult(culture, culture));
        }
    }
}
