﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace MyCompany.Resale.Presentation.Models.EventViewModel
{
    public class NewEventViewModel
    {
        [Required(ErrorMessage = "You must enter Event name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "You must enter satrt date")]
        public string DateStart { get; set; }
        [Required(ErrorMessage = "You must enter end date")]
        public string DateEnd { get; set; }
        [Required(ErrorMessage = "You must select banner image file")]
        public IFormFile Banner { get; set; }
        public string BannerPath { get; set; }
        [Required(ErrorMessage = "You must select Venue")]
        public int VenueId { get; set; }
        public string VenueName { get; set; }
        public int EventId { get; set; }
        [Required(ErrorMessage = "You must enter Description")]
        public string Description { get; set; }
        public List<City> AllCities { get; set; }
        public List<Venue> AllVenue { get; set; }
    }
}
