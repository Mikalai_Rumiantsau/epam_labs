﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Interface;
using System.Security.Claims;

namespace MyCompany.Resale.Presentation.Models
{
    public class UserStore : IUserPasswordStore<User>, IUserClaimStore<User>
    {
        private readonly IUserRepository _userRepository;
        private readonly IClaimRepository _claimRepository;
        public UserStore(IUserRepository userRepository, IClaimRepository claimRepository)
        {
            _userRepository = userRepository;
            _claimRepository = claimRepository;
        }
        public Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
        {
            var tempUser = FindByNameAsync(user?.Login.ToUpper(), CancellationToken.None);

            if (tempUser == null)
                return Task.FromResult(IdentityResult.Failed());

            _userRepository.Add(user);
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            var user = _userRepository.Users.Value.FirstOrDefault(u => u.Id == Convert.ToInt32(userId));

            return Task.FromResult(user);
        }

        public Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            var user = _userRepository.Users.Value.FirstOrDefault(u => u.Login == normalizedUserName);

            return Task.FromResult(user);
        }

        public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Login);
        }

        public Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id.ToString());
        }

        public Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Login);
        }

        public Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
        {
            user.Login = normalizedName;

            return Task.FromResult(0);
        }

        public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
        {
            var result = _userRepository.Update(user);
            if (result.IsFailure)
            {
                return Task.FromResult(IdentityResult.Failed());
            }

            return Task.FromResult(IdentityResult.Success);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
        {
            user.Password.Value = passwordHash;
            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Password.Value);
        }

        public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Password.Value != null);
        }

        public Task<IList<Claim>> GetClaimsAsync(User user, CancellationToken cancellationToken)
        {
            IList<Claim> claims = new List<Claim>();
            var userClaims = _claimRepository.Claims.Value.Where(c => c.UserId == user.Id);
            foreach (var uc in userClaims)
            {
                claims.Add(new Claim(uc.ClaimType, uc.ClailValue));
            }
            return Task.FromResult(claims);
        }

        public Task AddClaimsAsync(User user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            _claimRepository.Add(user, claims);

            return Task.CompletedTask;
        }

        public Task ReplaceClaimAsync(User user, Claim claim, Claim newClaim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task RemoveClaimsAsync(User user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IList<User>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
