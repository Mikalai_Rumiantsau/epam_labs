﻿using Microsoft.AspNetCore.Identity;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Provider;

namespace MyCompany.Resale.Presentation.Models
{
    public class PasswordHasher : IPasswordHasher<User>
    {
        public string HashPassword(User user, string password)
        {
            var hashedPassword = PasswordService.GetMD5Hash(password);
            user.Password.Value = hashedPassword;
            return hashedPassword;
        }
        public PasswordVerificationResult VerifyHashedPassword(User user, string hashedPassword,
            string providedPassword)
        {
            hashedPassword = user.Password.Value;
            if (hashedPassword == providedPassword)
            {
                return PasswordVerificationResult.Success;
            }
            else
            {
                return PasswordVerificationResult.Failed;
            }
        }
    }
}
