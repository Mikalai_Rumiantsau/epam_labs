﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Resale.Presentation.Models.CityViewModel
{
    public class NewCityViewModel
    {
        [Required(ErrorMessage = "You must enter city name")]
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
