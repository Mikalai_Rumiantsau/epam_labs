﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyCompany.Resale.Presentation.Models.TicketsViewModels
{
    public class NewTicketViewModel
    {
        [Required(ErrorMessage = "You must enter Price")]
        public int Price { get; set; }
        [Required(ErrorMessage = "You must enter Ticket Number")]
        public int Number { get; set; }
        public string Note { get; set; }
        [Required(ErrorMessage = "You must select Event")]
        public int EventId { get; set; }
        public string EventName { get; set; }
        public List<Event> AllEvents { get; set; }
        public string ReturnUrl { get; set; }
    }
}
