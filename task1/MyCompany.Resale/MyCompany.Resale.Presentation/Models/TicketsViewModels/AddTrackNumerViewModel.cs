﻿using System.ComponentModel.DataAnnotations;

namespace MyCompany.Resale.Presentation.Models.TicketsViewModels
{
    public class AddTrackNumerViewModel
    {
        public int OrderId { get; set; }
        [Required(ErrorMessage = "You must enter your Tracknumber")]
        public string TrackNumber { get; set; }
    }
}
