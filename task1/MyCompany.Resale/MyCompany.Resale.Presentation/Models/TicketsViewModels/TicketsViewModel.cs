﻿using MyCompany.Resale.BusinessLogic.Description;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Resale.Presentation.Models.TicketsViewModels
{
    public class TicketsViewModel
    {
        public List<Ticket> SelingTickets { get; set; }
        public List<Ticket> WaitingConfirmationTiclets { get; set; }
        public List<Ticket> ConfirmedAndDelivery { get; set; }
        public List<Ticket> SoldTickets { get; set; }
    }
}
