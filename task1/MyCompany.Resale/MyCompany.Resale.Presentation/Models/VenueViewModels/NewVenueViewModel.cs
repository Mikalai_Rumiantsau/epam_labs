﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyCompany.Resale.Presentation.Models.VenueViewModels
{
    public class NewVenueViewModel
    {
        [Required(ErrorMessage = "You must enter venue name")]
        public string VenueName { get; set; }
        [Required(ErrorMessage = "You must enter venue address")]
        public string Address { get; set; }
        public string CityName { get; set; }
        [Required(ErrorMessage = "You must select valid city address")]
        public int CityId { get; set; }
        public List<City> Cities { get; set; }
        public int VenueId { get; set; }
    }
}
