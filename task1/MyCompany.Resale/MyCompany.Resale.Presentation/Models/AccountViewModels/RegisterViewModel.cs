﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Resale.Presentation.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "The Login field is required.")]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "The E-mail field is required.")]
        [EmailAddress]
        public string Email { get; set; }
        [Display(Name = "Password")]

        [Required(ErrorMessage = "The Password field is required.")]
        [StringLength(100)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Localization")]
        [Required(ErrorMessage = "The Localization field is required.")]
        public string Localization { get; set; }
    }
}
