﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;

namespace MyCompany.Resale.Presentation.Models.AccountViewModels
{
    public class EditUserViewModel
    {
        public User User { get; set; }
        public int RoleId { get; set; }
        public List<Role> Roles { get; set; }
    }
}
