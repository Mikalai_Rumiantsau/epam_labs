﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyCompany.Resale.Presentation.Models.AccountViewModels
{
    public class ChangeRoleViewModel
    {
        public int UserId { get; set; }
        [Required(ErrorMessage = "No role")]
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public List<Role> Roles { get; set; }
    }
}
