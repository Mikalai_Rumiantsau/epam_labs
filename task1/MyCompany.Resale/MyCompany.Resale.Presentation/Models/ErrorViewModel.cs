﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Resale.Presentation.Models
{
    public class ErrorViewModel
    {
        public string ErrorMessage { get; set; }
        public string Owner { get; set; }
    }
}
