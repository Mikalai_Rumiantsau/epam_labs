﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;

namespace MyCompany.Resale.Presentation.Models
{
    public class DetailViewModel
    {
        public List<Ticket> Tickets { get; set; }
        public Event Event { get; set; }
    }
}
