﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyCompany.Resale.Presentation.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MyCompany.Resale.Presentation.Controllers
{
    public class ErrorController : Controller
    {
        [HttpGet]
        public IActionResult ShowError(string error)
        {
            var errorModel = new ErrorViewModel
            {
                ErrorMessage = error
            };
            return View(errorModel);
        }
    }
}
