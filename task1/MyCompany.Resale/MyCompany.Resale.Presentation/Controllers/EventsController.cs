﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using Microsoft.AspNetCore.Hosting;
using MyCompany.Resale.Presentation.Models.EventViewModel;
using MyCompany.Resale.BusinessLogic.Helpers;
using System.IO;
using Microsoft.Extensions.Logging;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MyCompany.Resale.Presentation.Controllers
{
 
    public class EventsController : Controller
    {
        private const string ImagePath = "/images/";
        private readonly IEventService _eventService;
        private readonly IHostingEnvironment _appEnvironment;
        private readonly ILogger _logger;

        public EventsController(
            ILogger<EventsController> loggerFactory,
            IEventService eventService, 
            IHostingEnvironment appEnvironment)
        {
            _eventService = eventService;
            _appEnvironment = appEnvironment;
            _logger = loggerFactory;
        }

        // GET: /<controller>/
        /// <summary>
        /// Main page, show all aviable events
        /// </summary>
        /// <returns></returns>

        public async Task<IActionResult> Index()
        {
            var events = _eventService.GetAllEvents();
            await events.OnFailure(() => ViewBag.Errors = events.Error);
            return View(events.Value);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult NewEvent()
        {
            var model = new NewEventViewModel();

            var cities = _eventService.GetAllCity();
            var venues = _eventService.GetAllVenue();

            if (Result.Combine(cities, venues).IsSuccess)
            {
                model.AllCities = cities.Value;
                model.AllVenue = venues.Value;

                return View(model);
            }

            _logger.LogError(cities.Error, venues.Error);
            return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                new { error = cities.Error, venues.Error });
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewEvent(NewEventViewModel model)
        {
            var cities = _eventService.GetAllCity();
            var venues = _eventService.GetAllVenue();
            var venue = _eventService.GetVenueById(model.VenueId);

            if (Result.Combine(cities, venues, venue).IsFailure)
            {
                _logger.LogError(cities.Error, venues.Error, venue.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = "" });
            }

            model.AllCities = cities.Value;
            model.AllVenue = venues.Value;

            if (ModelState.IsValid)
            {
                var dateConverter = new DateConverter();
                var path = SetUniqFileName(model.Banner.FileName);

                Result newEventResult = await _eventService.CreateEvent(
                        model.Name,
                        venue.Value,
                        dateConverter.GetDate(model.DateStart),
                        dateConverter.GetDate(model.DateEnd),
                        path,
                        model.Description)
                    .OnSuccess(e => _eventService.AddNewEvent(e))
                    .OnSuccess(() => SaveImage(path, model.Banner))
                    .OnBoth(r => r);

                if (newEventResult.IsFailure)
                {
                    _logger.LogError(newEventResult.Error);
                    ModelState.AddModelError("", newEventResult.Error);

                    return View(model);
                }

                return RedirectToAction(nameof(EventsController.Index), "Events");
            }

            return View(model);
        }

        /// <summary>
        ///  Set unique filename for save this file in folder without name collisions
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        /// /images/bcankixziijreeqztyyesmjswbbo.jpg
        /// /images/18-01-2017-T-14-51-24-E:\!training\epam_labs\task1\MyCompany.Resale\MyCompany.Resale.Presentation\wwwroot\images\vitaliy-sychev.jpg
        private static string SetUniqFileName(string fileName)
        {
            return (ImagePath + DateTime.Now.ToString("dd-MM-yyyy-T-HH-mm-ss-") + fileName);
        }


        /// <summary>
        /// Save file from filestream to drive
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileSent"></param>
        /// <returns></returns>
        private async Task<Result> SaveImage(string path, IFormFile fileSent)
        {
            try
            {
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await fileSent.CopyToAsync(fileStream);
                }

                return Result.Ok();
            }
            catch (DirectoryNotFoundException dirEx)
            {
                return Result.Fail(dirEx.Message);
            }
            catch (IOException ioEx)
            {
                return Result.Fail(ioEx.Message);
            }
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult EditEvent(int eventId)
        {
            var ev = _eventService.GetEventById(eventId);
            if (ev.IsFailure)
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = ev.Error });

            var model = new NewEventViewModel();

            var cities = _eventService.GetAllCity();
            var venues = _eventService.GetAllVenue();


            if (Result.Combine(cities, venues).IsSuccess)
            {
                model.EventId = ev.Value.Id;
                model.DateStart = ev.Value.DateStart.ToString("yyyy/MM/dd");
                model.DateEnd = ev.Value.DateEnd.ToString("yyyy/MM/dd");
                model.Description = ev.Value.Description;
                model.VenueId = ev.Value.Venue.Id;
                model.VenueName = $"{ev.Value.Venue.Name} ( {ev.Value.Venue.Address} )";
                model.Name = ev.Value.Name;
                model.AllCities = cities.Value;
                model.AllVenue = venues.Value;
                model.BannerPath = ev.Value.Banner;
                return View(model);
            }

            _logger.LogError(cities.Error, venues.Error);
            return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                new { error = cities.Error, venues.Error });
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditEvent(int eventId, NewEventViewModel model)
        {
            var ev = _eventService.GetEventById(eventId);
            var cities = _eventService.GetAllCity();
            var venues = _eventService.GetAllVenue();
            var venue = _eventService.GetVenueById(model.VenueId);

            var result = Result.Combine(cities, venues, venue, ev);
            if (result.IsFailure)
            {
                _logger.LogError(result.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = result.Error });
            }

            model.AllCities = cities.Value;
            model.AllVenue = venues.Value;

            if (ModelState.IsValid)
            {
                var dateConverter = new DateConverter();
                var path = SetUniqFileName(model.Banner.FileName);

                ev.Value.Name = model.Name;
                ev.Value.Venue = venue.Value;
                ev.Value.DateStart = dateConverter.GetDate(model.DateStart);
                ev.Value.DateEnd = dateConverter.GetDate(model.DateEnd);
                ev.Value.Banner = path;
                ev.Value.Description = model.Description;
                await SaveImage(path, model.Banner);

                var updateResult = _eventService.UpdateEvent(ev.Value);
                if (updateResult.IsFailure)
                {
                    _logger.LogError(ev.Error);
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                        new { error = updateResult.Error });
                }
                return RedirectToAction(nameof(HomeController.Tickets), "Home", new { id = ev.Value.Id });
            }

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteEvent(int eventId)
        {
            var deletResult = _eventService.GetEventById(eventId)
                .OnSuccess(e => _eventService.DeleteEvent(e));

            if (deletResult.IsFailure)
            {
                _logger.LogError(deletResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = deletResult.Error });
            }

            return RedirectToAction(nameof(EventsController.Index), "Events");
        }

        /// <summary>
        /// Wrapper around functions that add error to modelstate and log it.
        /// </summary>
        /// <param name="error"></param>
        private void AddModelStateError(string error)
        {
            _logger.LogError(error);
            ModelState.AddModelError("", error);
        }
    }
}
