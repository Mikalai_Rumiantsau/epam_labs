﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MyCompany.Resale.Presentation.Models.AccountViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Provider;
using Microsoft.Extensions.Logging;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.Threading;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MyCompany.Resale.Presentation.Controllers
{
    [AllowAnonymous]
        public class AccountController : Controller
        {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IStringLocalizer<AccountController> _localizer;
        private readonly IUserService _userService;
        private readonly IRoleStore<Role> _roleStore;
        private readonly ILogger _logger;

            public AccountController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IStringLocalizer<AccountController> localizer,
            ILogger<AccountController> loggerFactory, IUserService userService, IRoleStore<Role> roleStore)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _localizer = localizer;
            _userService = userService;
            _roleStore = roleStore;
            _logger = loggerFactory;
        }

        // GET: /Account/Login
        [HttpGet]
            [AllowAnonymous]
            public IActionResult Login(string returnUrl = null)
            {
            // ViewData["ReturnUrl"] = returnUrl;
            // return View();
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

            // POST: /Account/Login
            [HttpPost]
            [AllowAnonymous]
            [ValidateAntiForgeryToken]
            public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
            {
            if (ModelState.IsValid)
            {
                ViewData["ReturnUrl"] = returnUrl;
                string hashPassword = PasswordService.GetMD5Hash(model.Password);
                var result = await _signInManager.PasswordSignInAsync(model.Login, hashPassword, model.RememberMe, lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }
            return View(model);
            }

        //                    ModelState.AddModelError("", _localizer["Bad Login or(and) Password"]);

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if (ModelState.IsValid)
            {
                var user = new User
                {
                    Login = model.Login,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Localization = model.Localization,
                    Password = new PasswordHash { Value = model.Password },
                    Role = new Role { Name = "User" },
                    TwoFactorEnabled = false,
                    EmailConformed = false
                };

                var createResult = await _userManager.CreateAsync(user, model.Password);
                var userClaims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Role, user.Role.ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                };

                var claimResul = await _userManager.AddClaimsAsync(user, userClaims);

                if (createResult.Succeeded && claimResul.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3,
                        $"User created a new account with login: {user.Login}, password {user.Password}.");
                    return RedirectToLocal(returnUrl);
                }
                AddErrors(createResult);
                AddErrors(claimResul);
            }
            return View(model);
        }


        // GET: /Account/Register
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
            [ValidateAntiForgeryToken]
            public async Task<IActionResult> LogOff()
            {
                await _signInManager.SignOutAsync();
                return RedirectToAction(nameof(EventsController.Index), "Events");
            }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        [HttpGet]
        public IActionResult AllUsers()
        {
            var users = _userService.GetAllUsers();
            if (users.IsFailure)
            {
                _logger.LogError(users.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = users.Error });
            }

            return View(users.Value);
        }

        [HttpGet]
        public IActionResult EditUser(int userId)
        {
            if (!CheckUser(userId))
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = "Invalid user" });

            User user = _userManager.FindByIdAsync(userId.ToString()).Result;
            if (user == null)
            {
                _logger.LogError("We haven't this user");
                return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                    new { error = "We haven't this user" });
            }

            var model = new EditUserViewModel
            {
                User = user
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult EditUser(int userId, EditUserViewModel model)
        {
            if (!CheckUser(userId))
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = "Invalid user" });

            User user = _userManager.FindByIdAsync(userId.ToString()).Result;
            if (user == null)
            {
                _logger.LogError("We haven't this user");
                return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                    new { error = "We haven't this user" });
            }

            user.Address = model.User.Address;
            user.Email = model.User.Email;
            user.FirstName = model.User.FirstName;
            user.LastName = model.User.LastName;
            user.Login = model.User.Login;
            user.PhoneNmber = model.User.PhoneNmber;
            var result = _userManager.UpdateAsync(user);
            if (result.Result == IdentityResult.Success)
            {
               // HttpContext.Session.SetInt32("relogin", 1);
                return RedirectToAction(nameof(AccountController.CustomUserInfo), "Account", new { userId = user.Id });
            }
            return View(model);
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult ChangeRole(int userId)
        {
            User user = _userManager.FindByIdAsync(userId.ToString()).Result;
            if (user == null)
            {
                _logger.LogError("We haven't this user");
                return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                    new { error = "We haven't this user" });
            }

            var roles = _userService.GetAllRole();
            if (roles.IsFailure)
            {
                _logger.LogError(roles.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = roles.Error });
            }

            var model = new ChangeRoleViewModel
            {
                RoleName = user.Role.Name,
                UserId = user.Id,
                Roles = roles.Value
            };

            return View(model);
        }


        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult ChangeRole(int userId, ChangeRoleViewModel model)
        {
            User user = _userManager.FindByIdAsync(userId.ToString()).Result;
            if (user == null)
            {
                _logger.LogError("We haven't this user");
                return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                    new { error = "We haven't this user" });
            }

            var role = _roleStore.FindByIdAsync(model.RoleId.ToString(), CancellationToken.None).Result;
            if (role == null)
            {
                _logger.LogError("We haven't this role");
                return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                    new { error = "We haven't this role" });
            }

            var result = _userService.UpdateUserRole(user, role);
            if (result.IsFailure)
            {
                _logger.LogError(result.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = result.Error });
            }

            return RedirectToAction(nameof(AccountController.CustomUserInfo), "Account", new { userId = user.Id });
        }

        /// <summary>
        /// Helper methof for reditect to custom cation
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(EventsController.Index), "Events");
            }
        }

        /// <summary>
        /// Display full information about current authorized user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult CustomUserInfo(int userId)
        {
            var user = _userManager.FindByIdAsync(userId.ToString()).Result;
            if (user != null)
            {
                return View("UserInfo", user);
            }
            _logger.LogError("We haven't this user");
            return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = "We haven't this user" });
        }
        private bool CheckUser(int userId)
        {
            if (User.IsInRole("Admin"))
                return true;

            User user = _userManager.FindByNameAsync(User.Identity.Name).Result;

            return (user.Id == userId);
        }



    }
}
