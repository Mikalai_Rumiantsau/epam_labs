﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using MyCompany.Resale.BusinessLogic.Description;
using CSharpFunctionalExtensions;
using MyCompany.Resale.Presentation.Models.CityViewModel;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MyCompany.Resale.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CityController : Controller
    {
        private readonly ICityService _cityService;
        private readonly ILogger _logger;

        public CityController(
            IEventService eventService,
            ITicketService ticketService,
            IOrderService orderService,
            UserManager<User> userManager,
            ILogger<CityController> loggerFactory,
            ICityService cityService)
        {
            _cityService = cityService;
            _logger = loggerFactory;
        }


        [HttpGet]
        public IActionResult AllCities()
        {
            var cicies = _cityService.GetAllCities();
            if (cicies.IsFailure)
            {
                _logger.LogError(cicies.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = cicies.Error });
            }
            return View(cicies.Value);
        }

        [HttpGet]
        public IActionResult DeleteCity(int cityId)
        {
            var deleteResult = _cityService.GetCityById(cityId)
                .OnSuccess(c => _cityService.DeleteCity(c));

            if (deleteResult.IsFailure)
            {
                _logger.LogError(deleteResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = deleteResult.Error });
            }

            return RedirectToAction(nameof(CityController.AllCities));
        }

        [HttpGet]
        public IActionResult CraeteNewCity()
        {
            return View();
        }


        [HttpPost]
        public IActionResult CraeteNewCity(NewCityViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newCity = new City { Name = model.Name };

                var city = _cityService.GetCityByName(model.Name);
                if (city.IsSuccess)
                {
                    _logger.LogError("We have such City in DB");
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = "We have such City in DB" });
                }

                var addResult = _cityService.AddCity(newCity);

                if (addResult.IsFailure)
                {
                    _logger.LogError(addResult.Error);
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = addResult.Error });
                }

                return RedirectToAction(nameof(CityController.AllCities));
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult EditCity(int cityId)
        {
            var city = _cityService.GetCityById(cityId);
            if (city.IsFailure)
            {
                _logger.LogError(city.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = city.Error });
            }
            var model = new NewCityViewModel
            {
                Id = city.Value.Id,
                Name = city.Value.Name
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult EditCity(int cityId, NewCityViewModel model)
        {
            var city = _cityService.GetCityById(cityId);
            if (city.IsFailure)
            {
                _logger.LogError(city.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = city.Error });
            }
            city.Value.Name = model.Name;
            var updateResult = _cityService.UpdateCity(city.Value);
            if (updateResult.IsFailure)
            {
                _logger.LogError(updateResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = updateResult.Error });
            }
            return RedirectToAction(nameof(CityController.AllCities));
        }
    }
}
