﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MyCompany.Resale.Presentation.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IOrderService _orderService;

        public OrdersController(IEventService eventService, ITicketService ticketService, IOrderService orderService,
            UserManager<User> userManager)
        {
            _orderService = orderService;
            _userManager = userManager;
        }

        /// <summary>
        /// Display all orders curren authorize user
        /// </summary>
        /// <returns></returns>
        public IActionResult Orders()
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;

            if (user != null)
            {
                var orders = _orderService.GetOrdersByUserId(user.Id);
                if (orders.IsFailure)
                {
                    ViewBag.Errors = orders.Error;
                }
                return View(orders.Value);
            }

            return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = "This user isn't present in db" });
        }
    }
}
