﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.Presentation.Models.TicketsViewModels;
using CSharpFunctionalExtensions;
using System;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Security.Claims;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MyCompany.Resale.Presentation.Controllers
{
    [Authorize]
    public class TicketsController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IEventService _eventService;
        private readonly ITicketService _ticketService;
        private readonly IOrderService _orderService;
        private readonly ILogger _logger;

        public TicketsController(
            IEventService eventService,
            ITicketService ticketService,
            IOrderService orderService,
            UserManager<User> userManager,
            ILogger<TicketsController> loggerFactory)
        {
            _eventService = eventService;
            _ticketService = ticketService;
            _orderService = orderService;
            _userManager = userManager;
            _logger = loggerFactory;
        }

        /// <summary>
        /// Show all aviable ticket for autorized user
        /// </summary>
        /// <param name="state"> Show ststus of previous action (onfirm, delete...)</param>
        /// <returns></returns>
        public IActionResult Tickets(string state = null)
        {
            var id = Convert.ToInt32(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);

            var myTickets = new UserTicketsViewModel
            {
                SelingTickets = _ticketService.GetTicketsByStatus(TicketStatus.Selling, id).Value,
                WaitingConfirmationTiclets = _ticketService.GetTicketsByStatus(TicketStatus.WaitingConfirmation, id)
                    .Value,
                ConfirmedAndDelivery = _ticketService.GetTicketsByStatus(TicketStatus.ConfirmedAndDelivery).Value,
                SoldTickets = _ticketService.GetTicketsByStatus(TicketStatus.Sold, id).Value
            };

            ViewBag.TicketResult = state;

            return View(myTickets);
        }


        [HttpGet]
        public IActionResult NewTicket(string eventName, int eventId)
        {
            var events = _eventService.GetAllEvents();

            if (events.IsFailure)
            {
                _logger.LogError(events.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = events.Error });
            }
            var model = new NewTicketViewModel()
            {
                AllEvents = events.Value,
                EventName = eventName,
                EventId = eventId
            };
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult NewTicket(NewTicketViewModel model, string returnUrl = null)
        {
            var events = _eventService.GetAllEvents();
            if (events.IsFailure)
            {
                _logger.LogError(events.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = events.Error });
            }

            model.AllEvents = events.Value;

            if (ModelState.IsValid)
            {
                var ev = _eventService.GetEventById(model.EventId);
                var seller = _userManager.FindByNameAsync(User.Identity.Name).Result;
                if (ev.IsSuccess && seller != null)
                {
                    var ticket = new Ticket
                    {
                        Number = model.Number,
                        Price = model.Price,
                        Status = TicketStatus.Selling,
                        Note = model.Note,
                        Event = ev.Value,
                        Seller = seller
                    };

                    if (_ticketService.AddNewTicket(ticket).IsFailure)
                    {
                        ModelState.AddModelError("", "We have same ticket in database!");
                        return View(model);
                    }

                    return RedirectToAction(nameof(HomeController.Tickets), "Home", new { id = ticket.Event.Id });
                }
            }
            return View(model);
        }


        [HttpGet]
        public IActionResult BuyTicket(int ticketId)
        {
            var ticket = _ticketService.GetTicketById(ticketId);

            if (ticket.IsFailure)
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = ticket.Error });

            return View(ticket.Value);
        }


        [HttpPost]
        [ActionName("BuyTicket")]
        [ValidateAntiForgeryToken]
        public IActionResult BuyTicket_Post(int ticketId)
        {
            var ticket = _ticketService.GetTicketById(ticketId);
            if (ticket.IsFailure)
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = ticket.Error });

            var order = _orderService.GetOrderByTicketId(ticketId);
            if (order.IsSuccess)
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = "You have already booked the ticket" });

            User user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            if (user == null)
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = "Invalid user" });

            ticket.Value.Status = TicketStatus.WaitingConfirmation;

            var ticketUpdateResult = _ticketService.UpdateTicket(ticket.Value);
            var orderResult = _orderService.AddOrder(ticket.Value, user);
            var commonResult = Result.Combine(ticketUpdateResult, orderResult);

            if (commonResult.IsFailure)
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = commonResult.Error });

            return RedirectToAction(nameof(OrdersController.Orders), "Orders");
        }


        [HttpGet]
        public IActionResult ConfirmTicket(int ticketId)
        {
            var ticket = _ticketService.GetTicketById(ticketId);
            var order = _orderService.GetOrderByTicketId(ticketId);

            var commonUpdateResult = Result.Combine(ticket, order)
                .OnSuccess(() => _ticketService.SetStatus(ticket.Value, TicketStatus.ConfirmedAndDelivery))
                .OnSuccess(() => _orderService.SetStatus(order.Value, OrderStatus.Confirmed))
                .OnSuccess(func => _ticketService.TransactionUpdateTicket(ticket.Value, order.Value));

            if (commonUpdateResult.IsFailure)
            {
                _logger.LogError(commonUpdateResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                    new { error = commonUpdateResult.Error });
            }

            return RedirectToAction(nameof(TicketsController.Tickets), "Tickets", new { state = true });
        }


        [HttpGet]
        public IActionResult AddTrackNumber(int ticketId)
        {
            var order = _orderService.GetOrderByTicketId(ticketId);
            if (order.IsFailure)
            {
                _logger.LogError(order.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = order.Error });
            }

            var model = new AddTrackNumerViewModel
            {
                OrderId = order.Value.Id,
                TrackNumber = order.Value.TrackNumber
            };

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddTrackNumber(AddTrackNumerViewModel model, int orderId)
        {
            var result = _orderService.GetOrderById(orderId)
                .OnSuccess(o => _orderService.SetTrackNumber(o, model.TrackNumber))
                .OnSuccess(o => _orderService.UpdateOrder(o));
            if (result.IsFailure)
            {
                _logger.LogError(result.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = result.Error });
            }

            return RedirectToAction(nameof(TicketsController.Tickets), "Tickets");
        }


        [HttpGet]
        public IActionResult SoldTicket(int ticketId)
        {
            var ticket = _ticketService.GetTicketById(ticketId);
            var order = _orderService.GetOrderByTicketId(ticketId);

            var commonUpdateResult = Result.Combine(ticket, order)
                .OnSuccess(() => _ticketService.SetStatus(ticket.Value, TicketStatus.Sold))
                .OnSuccess(() => _orderService.SetStatus(order.Value, OrderStatus.Complete))
                .OnSuccess(r => _ticketService.TransactionUpdateTicket(ticket.Value, order.Value));

            if (commonUpdateResult.IsFailure)
            {
                _logger.LogError(commonUpdateResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                    new { error = commonUpdateResult.Error });
            }

            return RedirectToAction(nameof(Tickets));
        }


        [HttpGet]
        public IActionResult TicketInfo(int ticketId)
        {
            var ticket = _ticketService.GetTicketById(ticketId);

            return View(ticket.Value);
        }


        [HttpGet]
        public IActionResult RejectTicket(int ticketId)
        {
            var order = _orderService.GetOrderByTicketId(ticketId);
            var ticket = _ticketService.GetTicketById(ticketId);

            var getRsult = Result.Combine(ticket, order);
            if (getRsult.IsFailure)
            {
                _logger.LogError(getRsult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = getRsult.Error });
            }

            return View(order.Value);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RejectTicket(Order order)
        {
            var tOrder = _orderService.GetOrderById(order.Id);
            var ticket = _ticketService.GetTicketById(tOrder.Value.Ticket.Id);
            var accessResult = UserHaseTicket(tOrder.Value.Ticket.Id);

            var commonUpdateResult = Result.Combine(ticket, tOrder, accessResult)
                .OnSuccess(() => _ticketService.SetStatus(ticket.Value, TicketStatus.Selling))
                .OnSuccess(() => _orderService.SetStatus(tOrder.Value, OrderStatus.Rejected))
                .OnSuccess(() => _orderService.SetNote(tOrder.Value, order.Note))
                .OnSuccess(func => _ticketService.TransactionUpdateTicket(ticket.Value, tOrder.Value));

            if (commonUpdateResult.IsFailure)
            {
                _logger.LogError(commonUpdateResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                    new { error = commonUpdateResult.Error });
            }

            return RedirectToAction(nameof(Tickets));
        }

        [HttpGet]
        public IActionResult DeleteTicket(int ticketId)
        {
            var ticket = _ticketService.GetTicketById(ticketId);
            var accessResult = UserHaseTicket(ticketId);

            var deleteResult = Result.Combine(ticket, accessResult)
                .OnSuccess(() => _ticketService.Delete(ticket.Value));

            if (deleteResult.IsFailure)
            {
                _logger.LogError(deleteResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = deleteResult.Error });
            }
            return RedirectToAction(nameof(Tickets));
        }

        [HttpGet]
        public IActionResult EditTicket(int ticketId)
        {
            var ticket = _ticketService.GetTicketById(ticketId);
            var accessResult = UserHaseTicket(ticketId);
            var events = _eventService.GetAllEvents();

            var commonResult = Result.Combine(ticket, accessResult, events);
            if (commonResult.IsFailure)
            {
                _logger.LogError(commonResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                    new { error = commonResult.Error });
            }

            var model = new NewTicketViewModel()
            {
                AllEvents = events.Value,
                EventName = ticket.Value.Event.Name,
                EventId = ticket.Value.Event.Id,
                Note = ticket.Value.Note,
                Number = ticket.Value.Number,
                Price = ticket.Value.Price
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult EditTicket(int ticketId, NewTicketViewModel model)
        {
            if (ModelState.IsValid)
            {
                var ticket = _ticketService.GetTicketById(ticketId);
                var accessResult = UserHaseTicket(ticketId);
                var ev = _eventService.GetEventById(model.EventId);

                var commonResult = Result.Combine(ticket, accessResult, ev);
                if (commonResult.IsFailure)
                {
                    _logger.LogError(commonResult.Error);
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error",
                        new { error = commonResult.Error });
                }

                ticket.Value.Event = ev.Value;
                ticket.Value.Note = model.Note;
                ticket.Value.Number = model.Number;
                ticket.Value.Price = model.Price;

                var updateResult = _ticketService.UpdateTicket(ticket.Value);
                if (updateResult.IsFailure)
                {
                    _logger.LogError(updateResult.Error);
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = updateResult.Error });
                }

                return RedirectToAction(nameof(Tickets));
            }

            var events = _eventService.GetAllEvents();
            if (events.IsFailure)
            {
                _logger.LogError(events.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = events.Error });
            }
            model.AllEvents = events.Value;
            return View(model);
        }

        private Result UserHaseTicket(int ticketId)
        {
            User user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            if (user == null)
                return Result.Fail("Invalid user.");

            var tickets = _ticketService.GetTicketsByUser(user);
            if (tickets.IsFailure)
                return Result.Fail(tickets.Error);

            var ticket = tickets.Value.FirstOrDefault(t => t.Id == ticketId);
            if (ticket == null)
                return Result.Fail("Current user haven't such ticket.");
            return Result.Ok();
        }
    }
}