﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Identity;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using MyCompany.Resale.Presentation.Models;

namespace MyCompany.Resale.Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEventService _eventService;
        private readonly ITicketService _ticketService;

        public HomeController(IEventService eventService, ITicketService ticketService, IOrderService orderService,
            UserManager<User> userManager)
        {
            _eventService = eventService;
            _ticketService = ticketService;
        }

        public IActionResult Tickets(int id)
        {
            var tickets = _ticketService.GetTicketsByEevent(id);
            var ev = _eventService.GetEventById(id);

            if (ev.IsSuccess)
            {
                var datailModel = new DetailViewModel
                {
                    Tickets = tickets.Value,
                    Event = ev.Value
                };
                return View(datailModel);
            }

            return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = ev.Error });
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            string oldCulture = RouteData.Values["culture"].ToString();
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddMonths(1) }
            );
            string returnNewUrl;
            if (returnUrl == "/")
            {
                returnNewUrl = returnUrl+culture;
                Console.WriteLine("1  " + returnUrl+"         ::::    " + returnNewUrl);
            }
            else
            {
                returnNewUrl = ReplaceFirstOccurrence(returnUrl, oldCulture, culture);
                //returnNewUrl = returnUrl.Replace(oldCulture, culture,);
                Console.WriteLine("2  " + returnUrl + "         ::::    " + returnNewUrl);
            }
            RouteData.Values["culture"] = culture;
            return LocalRedirect($"{returnNewUrl}");
        }

        public string ReplaceFirstOccurrence(string Source, string Find, string Replace)
        {
            int Place = Source.IndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }
    }
}
