﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using Microsoft.Extensions.Logging;
using CSharpFunctionalExtensions;
using MyCompany.Resale.Presentation.Models.VenueViewModels;
using MyCompany.Resale.BusinessLogic.Description;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MyCompany.Resale.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class VenueController : Controller
    {
        private readonly ICityService _cityService;
        private readonly IVenueService _venueService;
        private readonly ILogger _logger;

        public VenueController(
            ILogger<VenueController> loggerFactory,
            ICityService cityService,
            IVenueService venueService)
        {
            _cityService = cityService;
            _venueService = venueService;
            _logger = loggerFactory;
        }
        
        [HttpGet]
        public IActionResult AllVenue()
        {
            var venues = _venueService.GetAllVenues();
            if (venues.IsFailure)
            {
                _logger.LogError(venues.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = venues.Error });
            }

            return View(venues.Value);
        }


        [HttpGet]
        public IActionResult DeleteVenue(int venueId)
        {
            var deleteResult = _venueService.GetVenueById(venueId)
                .OnSuccess(v => _venueService.DeleteVenue(v));

            if (deleteResult.IsFailure)
            {
                _logger.LogError(deleteResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = deleteResult.Error });
            }

            return RedirectToAction(nameof(VenueController.AllVenue));
        }

        [HttpGet]
        public IActionResult CreateNewVenue()
        {
            var cities = _cityService.GetAllCities();
            if (cities.IsFailure)
            {
                _logger.LogError(cities.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = cities.Error });
            }

            var model = new NewVenueViewModel
            {
                Cities = cities.Value
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult CreateNewVenue(NewVenueViewModel model)
        {
            if (ModelState.IsValid)
            {
                var city = _cityService.GetCityById(model.CityId);
                if (city.IsFailure)
                {
                    _logger.LogError(city.Error);
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = city.Error });
                }

                var tVenue = _venueService.GetVenueByName(model.VenueName);
                if (tVenue.IsSuccess)
                {
                    _logger.LogError("We have such City in DB");
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = "We have such City in DB" });
                }

                var venue = new Venue { Name = model.VenueName, Address = model.Address, City = city.Value };

                var addResult = _venueService.AddVenue(venue);
                if (addResult.IsFailure)
                {
                    _logger.LogError(tVenue.Error);
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = tVenue.Error });
                }

                return RedirectToAction(nameof(VenueController.AllVenue));
            }

            var cities = _cityService.GetAllCities();
            if (cities.IsFailure)
            {
                _logger.LogError(cities.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = cities.Error });
            }
            model.Cities = cities.Value;

            return View(model);
        }

        [HttpGet]
        public IActionResult EditVenue(int venueId)
        {
            var venue = _venueService.GetVenueById(venueId);
            var cities = _cityService.GetAllCities();

            var commonResult = Result.Combine(venue, cities);

            if (commonResult.IsFailure)
            {
                _logger.LogError(commonResult.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = commonResult.Error });
            }

            var model = new NewVenueViewModel
            {
                Address = venue.Value.Address,
                VenueName = venue.Value.Name,
                CityId = venue.Value.City.Id,
                CityName = venue.Value.City.Name,
                VenueId = venue.Value.Id,
                Cities = cities.Value
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult EditVenue(int venueId, NewVenueViewModel model)
        {
            if (ModelState.IsValid)
            {
                var venue = _venueService.GetVenueById(venueId);
                var city = _cityService.GetCityById(model.CityId);

                var commonResult = Result.Combine(venue, city);
                if (commonResult.IsFailure)
                {
                    _logger.LogError(commonResult.Error);
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = commonResult.Error });
                }

                venue.Value.Address = model.Address;
                venue.Value.Name = model.VenueName;
                venue.Value.City = city.Value;

                var updateResult = _venueService.UpdateVenue(venue.Value);
                if (updateResult.IsFailure)
                {
                    _logger.LogError(updateResult.Error);
                    return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = updateResult.Error });
                }

                return RedirectToAction(nameof(VenueController.AllVenue));
            }

            var cities = _cityService.GetAllCities();
            if (cities.IsFailure)
            {
                _logger.LogError(cities.Error);
                return RedirectToAction(nameof(ErrorController.ShowError), "Error", new { error = cities.Error });
            }

            model.Cities = cities.Value;
            return View(model);
        }
    }
}
