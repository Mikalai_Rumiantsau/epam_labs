using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly EFCDbContext _context;

        public UserRepository(EFCDbContext context)
        {
            _context = context;
        }

        public DbContext Context
        {
            get { return _context; }
        }

        public Result<IEnumerable<User>> Users
        {
            get
            {
                try
                {
                    return Result.Ok<IEnumerable<User>>(
                        _context.Users
                            .Include(u => u.Password)
                            .Include(t => t.Tickets)
                            .Include(o => o.Orders)
                            .Include(c => c.Claims)
                            .Include(r => r.Role));
                }
                catch (DbUpdateException)
                {
                    return Result.Fail<IEnumerable<User>>("Unable to open the DB connection");
                }
            }
        }

        public Result Add(User user)
        {
            try
            {
                _context.Add(user);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException)
            {
                return Result.Fail("Unable to open the DB connection");
            }
        }

        public Result Update(User user)
        {
            try
            {
                _context.Users.Update(user);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail(ex.Message);
            }
        }
    }
}