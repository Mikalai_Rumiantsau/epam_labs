using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using MyCompany.Resale.BusinessLogic.Interface;
using Microsoft.EntityFrameworkCore;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class CityRepository : ICityRepository
    {
        private readonly EFCDbContext _context;

        public CityRepository(EFCDbContext context)
        {
            _context = context;
        }

        public Result<IEnumerable<City>> Cities
        {
            get
            {
                try
                {
                    IEnumerable<City> cities = _context.Cities.Include(v => v.Venues);
                    return Result.Ok(cities);
                }
                catch (DbUpdateException e)
                {
                    return Result.Fail<IEnumerable<City>>("Unable to open the DB connection");
                }
            }
        }

        public Result Delte(City city)
        {
            try
            {
                _context.Cities.Remove(city);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public Result Add(City city)
        {
            try
            {
                _context.Cities.Add(city);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public Result Update(City city)
        {
            try
            {
                _context.Cities.Update(city);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail(ex.Message);
            }
        }
    }
}
 