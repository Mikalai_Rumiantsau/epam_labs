using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class EventRepository : IEventRepository
    {
        private readonly EFCDbContext _context;

        public EventRepository(EFCDbContext context)
        {
            _context = context;
        }

        public Result<IEnumerable<Event>> Events
        {
            get
            {
                try
                {
                    return Result.Ok<IEnumerable<Event>>(
                        _context.Events
                            .Include(e => e.Venue)
                            .ThenInclude(c => c.City));
                }
                catch (DbUpdateException)
                {
                    return Result.Fail<IEnumerable<Event>>("Unable to open the DB connection");
                }
            }
        }

        public Result Add(Event newEvent)
        {
            try
            {
                _context.Events.Add(newEvent);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException)
            {
                return Result.Fail("Unable to open the DB connection");
            }
        }

        public Result Update(Event ev)
        {
            try
            {
                _context.Events.Update(ev);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException)
            {
                return Result.Fail("Unable to open the DB connection");
            }
        }

        public Result Delete(Event ev)
        {
            try
            {
                _context.Events.Remove(ev);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail(ex.Message);
            }
        }
    }
}