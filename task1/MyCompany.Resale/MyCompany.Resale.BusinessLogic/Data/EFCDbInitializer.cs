using MyCompany.Resale.BusinessLogic.Provider;
using MyCompany.Resale.BusinessLogic.Description;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public static class EFCDbInitializer
    {
        public static void Initialize(EFCDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Cities.Any())
            {
                return;
            }

            // Cities
            var gomel = new City { Name = "Gomel" };
            var minsk = new City { Name = "Minsk" };
            var mogilev = new City { Name = "Mogilev" };
            var brest = new City { Name = "Brest" };
            var grodno = new City { Name = "Grodno" };
            var vitebsk = new City { Name = "Vitebsk" };

            var cities = new List<City>
            {
                gomel,
                minsk,
                mogilev,
                brest,
                grodno,
                vitebsk
            };
            context.Cities.AddRange(cities);
            context.SaveChanges();

            //Venues
        var cityCulturalCenter = new Venue
            {
                Name = "City Cultural Center",
                Address = "ul. Irininskaja, 16",
                City = gomel
            };
            var siti = new Venue
            {
                Name = "Siti",
                Address = "pr-t Oktjabrja, 46",
                City = gomel
            };
            var dvorecRumjancevyh = new Venue
            {
                Name = "Dvorec Rumjancevyh i Paskevichej",
                Address = "pl. Lenina, 4",
                City = gomel
            };
            var minskArena = new Venue
            {
                Name = "Minsk-Arena",
                Address = "pr-t Pobeditelej, 111",
                City = minsk
            };
            var rePublic = new Venue
            {
                Name = "Re:Public",
                Address = "ul. Prityckogo, 62",
                City = minsk
            };
            var dvorecRespubliki = new Venue
            {
                Name = "Dvorec Respubliki",
                Address = "pl. Oktjabr'skaja, 1",
                City = minsk
            };

            var palaceOfCultureArea = new Venue
            {
                Name = "Palace of culture area",
                Address = "pr-t Pushkina, 7",
                City = mogilev
            };
            var icePalace = new Venue
            {
                Name = "Ice Palace",
                Address = "ul. Gagarina, 1",
                City = mogilev
            };
            var concertHallOfMogilev = new Venue
            {
                Name = "Concert Hall of Mogilev",
                Address = "ul. Leninskaja 7",
                City = mogilev
            };
            var brestOblastPhilharmonicHall = new Venue
            {
                Name = "Brest oblast Philharmonic Hall",
                Address = "ul. Ordzhonikidze, 14",
                City = brest
            };
            var brestRegionalPalace = new Venue
            {
                Name = "Brest regional Palace of culture of the Federation of trade unions of Belarus.",
                Address = "Brest",
                City = brest
            };
            var brestAcademic = new Venue
            {
                Name = "Brest academic Theater of drama named Lenin Komsomol of Belarus",
                Address = "ul. Lenina, 21",
                City = brest
            };

            var sovietSquare = new Venue
            {
                Name = "Soviet square in Hrodna",
                Address = "pl. Sovetskaja",
                City = grodno
            };
            var grodnoPalace = new Venue
            {
                Name = "Grodno Palace Of Culture",
                Address = "ul. Sovetskaja, 6",
                City = grodno
            };
            var grodnoIce = new Venue
            {
                Name = "Grodno Ice Palace",
                Address = "ul. Kommunal'naja, 3A",
                City = grodno
            };

            var regionalPhilarmonic = new Venue
            {
                Name = "Regional Philarmonic Hall",
                Address = "pl. Lenina, 69",
                City = vitebsk
            };
            var gannaHave = new Venue
            {
                Name = "Ganna Have",
                Address = "pr-t Moskovskij, 99",
                City = vitebsk
            };
            var culturalHistorical = new Venue
            {
                Name = "Cultural-historical complex �golden ring of Vitebsk� Dvina�",
                Address = "ul. Chajkovskogo, 3",
                City = vitebsk
            };

            var venues = new List<Venue>()
            {
                cityCulturalCenter,
                siti,
                dvorecRumjancevyh,
                minskArena,
                rePublic,
                dvorecRespubliki,
                palaceOfCultureArea,
                icePalace,
                concertHallOfMogilev,
                brestOblastPhilharmonicHall,
                brestRegionalPalace,
                brestAcademic,
                sovietSquare,
                grodnoPalace,
                grodnoIce,
                regionalPhilarmonic,
                gannaHave,
                culturalHistorical
            };
            context.Venues.AddRange(venues);
            context.SaveChanges();

            //Roles
            var admin = new Role { Name = "Admin" };
            var user = new Role { Name = "User" };

            var roles = new List<Role>()
            {
                admin,
                user
            };
            context.Roles.AddRange(roles);
            context.SaveChanges();

            //Passwords
            var adminPassword = new PasswordHash
            {
                Value = PasswordService.GetMD5Hash("Admin")
            };
            var userPassword = new PasswordHash
            {
                Value = PasswordService.GetMD5Hash("User")
            };
            var kolyaPassword = new PasswordHash
            {
                Value = PasswordService.GetMD5Hash("Kolya")
            };

            var passwords = new List<PasswordHash>()
            {
                adminPassword,
                userPassword,
                kolyaPassword
            };
            context.Passwords.AddRange(passwords);

            //Users
            var userAdmin = new User
            {
                FirstName = "AdminFirstName",
                LastName = "AdminLastName",
                Email = "admin@mail.com",
                EmailConformed = true,
                PhoneNmber = "+565607264332",
                Address = "Gomel, st. Sovet 14",
                Localization = "en",
                Login = "ADMIN",
                Password = adminPassword,
                Role = admin,
                TwoFactorEnabled = false
            };
            var userUser = new User
            {
                FirstName = "UserFirstName",
                LastName = "UserLastName",
                Email = "user@mail.ru",
                EmailConformed = true,
                PhoneNmber = "+375447415577",
                Address = "Minsk, Nezavisimosti Pr. 52",
                Localization = "be",
                Login = "USER",
                Password = userPassword,
                Role = user,
                TwoFactorEnabled = false
            };
            var userKolya = new User
            {
                FirstName = "KolyaFirstName",
                LastName = "KolyaLastName",
                Email = "kolya@mail.ru",
                EmailConformed = false,
                PhoneNmber = "+375294228165",
                Address = "Minsk, Mokhovaya Street 21",
                Localization = "ru",
                Login = "KOLYA",
                Password = kolyaPassword,
                Role = user,
                TwoFactorEnabled = false
            };

            var users = new List<User>()
            {
                userAdmin,
                userUser,
                userKolya
            };
            context.Users.AddRange(users);
            context.SaveChanges();

            //Events
            var event1 = new Event
            {
                Name = "Vitaliy Sychev",
                DateStart = new DateTime(2017, 1, 17),
                DateEnd = new DateTime(2017, 1, 17),
                Banner = "/images/vitaliy-sychev.jpg",
                Description = "Will take place within the framework of the action �Tree of good deeds�, which runs from December 14 to January 15, 2017, at CMU. A portion of the proceeds from tickets sold will be credited to the needy sick children.",
                Status = EventStatus.Activ,
                Venue = cityCulturalCenter
            };
            var event2 = new Event
            {
                Name = "Novogodnie priklyucheniya",
                DateStart = new DateTime(2017, 2, 5),
                DateEnd = new DateTime(2017, 2, 5),
                Banner = "/images/novogodnie-priklyucheniya-788099.jpg",
                Description = "It is time to think about the new year. In the cosy and hospitable city caf? with 10 December till 8 January for children of different ages, we offer three totally different filling programme.",
                Status = EventStatus.Activ,
                Venue = siti
            };
            var event3 = new Event
            {
                Name = "Novogodniy utrennik",
                DateStart = new DateTime(2017, 2, 11),
                DateEnd = new DateTime(2017, 2, 11),
                Banner = "/images/novogodniy-utrennik-son-v.jpg",
                Description = "New year is time of miracles!",
                Status = EventStatus.Activ,
                Venue = dvorecRumjancevyh
            };

            var event4 = new Event
            {
                Name = "Ledovyy spektakl",
                DateStart = new DateTime(2017, 3, 1),
                DateEnd = new DateTime(2017, 3, 1),
                Banner = "/images/ledovyy-spektakl-karmen.jpg",
                Description = "World premiere! In Minsk come skating LEGENDS.Don't miss the musical 'Carmen'.",
                Status = EventStatus.Activ,
                Venue = minskArena
            };
            var event5 = new Event
            {
                Name = "Trubetskoy",
                DateStart = new DateTime(2017, 3, 15),
                DateEnd = new DateTime(2017, 3, 15),
                Banner = "/images/sovmestnyy-koncert-grupp-trubetskoy-i-elizium.jpg",
                Description = "Real rock music lovers should be 15 December the Club Re: public. After all the Group's songs �trubetskoy� can be called truly legendary: too much they invested too much associated with them.",
                Status = EventStatus.Activ,
                Venue = rePublic
            };
            var event6 = new Event
            {
                Name = "Glavnaya elochka",
                DateStart = new DateTime(2017, 4, 20),
                DateEnd = new DateTime(2017, 4, 20),
                Banner = "/images/glavnaya-elochka-strany.jpg",
                Description = "The Small Hall of the Palace of the Republic invites kids and their parents to the surprising New Year's fairy tale.",
                Status = EventStatus.Activ,
                Venue = dvorecRespubliki
            };
        
            var event7 = new Event
            {
                Name = "Skazki venskogo lesa",
                DateStart = new DateTime(2017, 5, 12),
                DateEnd = new DateTime(2017, 5, 12),
                Banner = "/images/skazki-venskogo-lesa.jpg",
                Description = "If you notice an error in the text of the news, please select it and press Ctrl + Enter",
                Status = EventStatus.Activ,
                Venue = palaceOfCultureArea
            };
            var event8 = new Event
            {
                Name = "Alisa v strane chudes na ldu",
                DateStart = new DateTime(2017, 6, 17),
                DateEnd = new DateTime(2017, 6, 17),
                Banner = "/images/alisa-v-strane-chudes-na-ldu.jpg",
                Description = "In Mogilev will show ice show Helena Berezhnoy �Alice in Wonderland�",
                Status = EventStatus.Activ,
                Venue = icePalace
            };
            var event9 = new Event
            {
                Name = "Priklyucheniya lvenka kuzi v snezhnom korolevstve",
                DateStart = new DateTime(2017, 7, 21),
                DateEnd = new DateTime(2017, 7, 21),
                Banner = "/images/priklyucheniya-lvenka-kuzi-v-snezhnom-korolevstve-931820.jpg",
                Description = "If you notice an error in the text of the news, please select it and press Ctrl + Enter",
                Status = EventStatus.Activ,
                Venue = concertHallOfMogilev
            };

            var event10 = new Event
            {
                Name = "Kalyadny raut z zolki band",
                DateStart = new DateTime(2017, 8, 25),
                DateEnd = new DateTime(2017, 8, 25),
                Banner = "/images/kalyadny-raut-z-zolki-band.jpg",
                Description = "Syabry! We want a sustrjecca w vam� in new, non-standard farmace.",
                Status = EventStatus.Activ,
                Venue = brestOblastPhilharmonicHall
            };

            var event11 = new Event
            {
                Name = "Novogodniy perepolokh ili tayna karabasa barabasa",
                DateStart = new DateTime(2017, 2, 5),
                DateEnd = new DateTime(2017, 2, 5),
                Banner = "/images/novogodniy-perepolokh-ili-tayna-karabasa-barabasa.jpg",
                Description = "Guys expects a fascinating presentation of new trouble or Mystery Theatre Barabasa�. Plays begin at 11.00 and 14.00.",
                Status = EventStatus.Activ,
                Venue = brestRegionalPalace
            };
                        
            var event12 = new Event
            {
                Name = "Yanvarskie muzykalnye vechera",
                DateStart = new DateTime(2017, 2, 5),
                DateEnd = new DateTime(2017, 2, 5),
                Banner = "/images/yanvarskie-muzykalnye-vechera-torzhestvennoe-otkrytie-festivalya-481171.jpg",
                Description = "If you notice an error in the text of the news, please select it and press Ctrl + Enter",
                Status = EventStatus.Activ,
                Venue = brestAcademic
            };

            var event13 = new Event
            {
                Name = "Kazhdyy v grodno videt rad etot skazochnyy parad",
                DateStart = new DateTime(2017, 2, 5),
                DateEnd = new DateTime(2017, 2, 5),
                Banner = "/images/kazhdyy-v-grodno-videt-rad-etot-skazochnyy-parad.jpg",
                Description = "December 16, 2016 in Grodno will host a festive theatrical procession of father frosts and snegurochkas everyone in Grodno see glad this fabulous parade.",
                Status = EventStatus.Activ,
                Venue = sovietSquare
            };


            var event14 = new Event
            {
                Name = "Muzyka rukhu maladaya polshcha",
                DateStart = new DateTime(2017, 2, 5),
                DateEnd = new DateTime(2017, 2, 5),
                Banner = "/images/muzyka-rukhu-maladaya-polshcha.jpg",
                Description = "Ticket sales-the Palace of culture, PL Sovetskaya, 6",
                Status = EventStatus.Activ,
                Venue = grodnoPalace
            };

            var event15 = new Event
            {
                Name = "Shou dlya vsey semi alisa v strane chudes na ldu",
                DateStart = new DateTime(2017, 2, 5),
                DateEnd = new DateTime(2017, 2, 5),
                Banner = "/images/shou-dlya-vsey-semi-alisa-v-strane-chudes-na-ldu-891694.jpg",
                Description = "Than to please yourself and your kids on new year's Eve? Of course, the fabulous ice show based on favorite fairy tales!",
                Status = EventStatus.Activ,
                Venue = grodnoIce
            };
            
            var event16 = new Event
            {
                Name = "Lyubvi negromkie slova",
                DateStart = new DateTime(2017, 2, 5),
                DateEnd = new DateTime(2017, 2, 5),
                Banner = "/images/lyubvi-negromkie-slova.jpg",
                Description = "Like the concert? Share with your friends!",
                Status = EventStatus.Activ,
                Venue = regionalPhilarmonic
            };

            var event17 = new Event
            {
                Name = "Bcankixzi i jreeq",
                DateStart = new DateTime(2017, 2, 5),
                DateEnd = new DateTime(2017, 2, 5),
                Banner = "/images/bcankixziijreeqztyyesmjswbbo.jpg",
                Description = "Every Thursday the restaurant \"Hanna\" invites you to an evening of jazz. Live sound.",
                Status = EventStatus.Activ,
                Venue = gannaHave
            };

            var event18 = new Event
            {
                Name = "Programma dlya detey 'perezagruzka'",
                DateStart = new DateTime(2017, 2, 5),
                DateEnd = new DateTime(2017, 2, 5),
                Banner = "/images/programma-dlya-detey-perezagruzka-9381433.jpg",
                Description = "Log in to leave a comment.",
                Status = EventStatus.Activ,
                Venue = culturalHistorical
            };

            var events = new List<Event>()
            {
                event1,
                event2,
                event3,
                event4,
                event5,
                event6,
                event7,
                event8,
                event9,
                event10,
                event11,
                event12,
                event13,
                event14,
                event15,
                event16,
                event17,
                event18
            };
            context.Events.AddRange(events);
            context.SaveChanges();

            //Tickets
            var ticket1 = new Ticket
            {
                Event = event1,
                Number = 22,
                Price = 155,
                Seller = userAdmin,
                Status = TicketStatus.WaitingConfirmation
            };
            var ticket2 = new Ticket
            {
                Event = event1,
                Number = 23,
                Price = 155,
                Seller = userAdmin,
                Status = TicketStatus.WaitingConfirmation
            };
            var ticket3 = new Ticket
            {
                Event = event2,
                Number = 77,
                Price = 210,
                Seller = userAdmin,
                Status = TicketStatus.Delivery
            };
            var ticket4 = new Ticket
            {
                Event = event2,
                Number = 122,
                Price = 525,
                Seller = userAdmin,
                Status = TicketStatus.ConfirmedAndDelivery
            };
            var ticket5 = new Ticket
            {
                Event = event3,
                Number = 2,
                Price = 55,
                Seller = userAdmin,
                Status = TicketStatus.Selling
            };
            var ticket6 = new Ticket
            {
                Event = event3,
                Number = 22,
                Price = 15,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket7 = new Ticket
            {
                Event = event4,
                Number = 12,
                Price = 15,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket8 = new Ticket
            {
                Event = event4,
                Number = 222,
                Price = 355,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket9 = new Ticket
            {
                Event = event5,
                Number = 221,
                Price = 355,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket10 = new Ticket
            {
                Event = event5,
                Number = 32,
                Price = 35,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket11 = new Ticket
            {
                Event = event6,
                Number = 12,
                Price = 60,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket12 = new Ticket
            {
                Event = event6,
                Number = 52,
                Price = 101,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket13 = new Ticket
            {
                Event = event7,
                Number = 52,
                Price = 62,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket14 = new Ticket
            {
                Event = event7,
                Number = 203,
                Price = 82,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket15 = new Ticket
            {
                Event = event8,
                Number = 230,
                Price = 85,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket16 = new Ticket
            {
                Event = event8,
                Number = 123,
                Price = 90,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket17 = new Ticket
            {
                Event = event9,
                Number = 124,
                Price = 90,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket18 = new Ticket
            {
                Event = event9,
                Number = 134,
                Price = 55,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket19 = new Ticket
            {
                Event = event10,
                Number = 234,
                Price = 65,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket20 = new Ticket
            {
                Event = event10,
                Number = 32,
                Price = 70,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket21 = new Ticket
            {
                Event = event11,
                Number = 33,
                Price = 70,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket22 = new Ticket
            {
                Event = event11,
                Number = 72,
                Price = 99,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket23 = new Ticket
            {
                Event = event12,
                Number = 109,
                Price = 89,
                Seller = userUser,
                Status = TicketStatus.Selling
            };

            var ticket24 = new Ticket
            {
                Event = event12,
                Number = 22,
                Price = 155,
                Seller = userAdmin,
                Status = TicketStatus.WaitingConfirmation
            };
            var ticket25 = new Ticket
            {
                Event = event13,
                Number = 23,
                Price = 155,
                Seller = userAdmin,
                Status = TicketStatus.WaitingConfirmation
            };
            var ticket26 = new Ticket
            {
                Event = event13,
                Number = 77,
                Price = 210,
                Seller = userAdmin,
                Status = TicketStatus.Delivery
            };
            var ticket27 = new Ticket
            {
                Event = event14,
                Number = 122,
                Price = 525,
                Seller = userAdmin,
                Status = TicketStatus.ConfirmedAndDelivery
            };
            var ticket28 = new Ticket
            {
                Event = event14,
                Number = 2,
                Price = 55,
                Seller = userAdmin,
                Status = TicketStatus.Selling
            };
            var ticket29 = new Ticket
            {
                Event = event15,
                Number = 22,
                Price = 15,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket30 = new Ticket
            {
                Event = event15,
                Number = 12,
                Price = 15,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket31 = new Ticket
            {
                Event = event16,
                Number = 222,
                Price = 355,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket32 = new Ticket
            {
                Event = event16,
                Number = 221,
                Price = 355,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket33 = new Ticket
            {
                Event = event17,
                Number = 32,
                Price = 35,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket34 = new Ticket
            {
                Event = event17,
                Number = 12,
                Price = 60,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket35 = new Ticket
            {
                Event = event18,
                Number = 52,
                Price = 101,
                Seller = userUser,
                Status = TicketStatus.Selling
            };
            var ticket36 = new Ticket
            {
                Event = event18,
                Number = 52,
                Price = 62,
                Seller = userUser,
                Status = TicketStatus.Selling
            };

            var tickets = new List<Ticket>()
            {
                ticket1,
                ticket2,
                ticket3,
                ticket4,
                ticket5,
                ticket6,
                ticket7,
                ticket8,
                ticket9,
                ticket10,
                ticket11,
                ticket12,
                ticket13,
                ticket14,
                ticket15,
                ticket16,
                ticket17,
                ticket18,
                ticket19,
                ticket20,
                ticket21,
                ticket22,
                ticket23,
                ticket24,
                ticket25,
                ticket26,
                ticket27,
                ticket28,
                ticket29,
                ticket30,
                ticket31,
                ticket32,
                ticket33,
                ticket34,
                ticket35,
                ticket36
            };
            context.Tickets.AddRange(tickets);
            context.SaveChanges();

            //Orders
            var order1 = new Order
            {
                Ticket = ticket1,
                Buyer = userUser,
                Status = OrderStatus.Waiting
            };
            var order2 = new Order
            {
                Ticket = ticket2,
                Buyer = userUser,
                Status = OrderStatus.Waiting
            };
            var order3 = new Order
            {
                Ticket = ticket3,
                Buyer = userUser,
                Status = OrderStatus.Confirmed,
                TrackNumber = "AB122382374E"
            };
            var order4 = new Order
            {
                Ticket = ticket4,
                Buyer = userUser,
                Status = OrderStatus.Confirmed
            };
            var order5 = new Order
            {
                Ticket = ticket5,
                Buyer = userUser,
                Status = OrderStatus.Rejected
            };
            var order6 = new Order
            {
                Ticket = ticket6,
                Buyer = userUser,
                Status = OrderStatus.Waiting
            };
            var order7 = new Order
            {
                Ticket = ticket7,
                Buyer = userUser,
                Status = OrderStatus.Waiting
            };
            var order8 = new Order
            {
                Ticket = ticket8,
                Buyer = userUser,
                Status = OrderStatus.Confirmed,
                TrackNumber = "AB122382374E"
            };
            var order9 = new Order
            {
                Ticket = ticket9,
                Buyer = userUser,
                Status = OrderStatus.Confirmed
            };
            var order10 = new Order
            {
                Ticket = ticket10,
                Buyer = userUser,
                Status = OrderStatus.Rejected
            };

            var orders = new List<Order>()
            {
                order1,
                order2,
                order3,
                order4,
                order5,
                order6,
                order7,
                order8,
                order9,
                order10
            };
            context.AddRange(orders);
            context.SaveChanges();
            //Claims
            var adminCliamRole = new UserClaim
            {
                ClaimType = ClaimTypes.Role,
                ClailValue = "Admin",
                User = userAdmin,
                UserId = userAdmin.Id
            };
            var adminCliamId = new UserClaim
            {
                ClaimType = ClaimTypes.NameIdentifier,
                ClailValue = userAdmin.Id.ToString(),
                User = userAdmin,
                UserId = userAdmin.Id
            };
            var userCliamRole = new UserClaim
            {
                ClaimType = ClaimTypes.Role,
                ClailValue = "User",
                User = userUser,
                UserId = userUser.Id
            };
            var userCliamId = new UserClaim
            {
                ClaimType = ClaimTypes.NameIdentifier,
                ClailValue = userUser.Id.ToString(),
                User = userUser,
                UserId = userUser.Id
            };
            var kolyaCliamRole = new UserClaim
            {
                ClaimType = ClaimTypes.Role,
                ClailValue = "User",
                User = userKolya,
                UserId = userKolya.Id
            };
            var kolyaCliamId = new UserClaim
            {
                ClaimType = ClaimTypes.NameIdentifier,
                ClailValue = userKolya.Id.ToString(),
                User = userKolya,
                UserId = userKolya.Id
            };

            var usersClaims = new List<UserClaim>()
            {
                adminCliamRole,
                adminCliamId,
                userCliamRole,
                userCliamId,
                kolyaCliamRole,
                kolyaCliamId
            };
            context.AddRange(usersClaims);
            context.SaveChanges();
        }
    }
}