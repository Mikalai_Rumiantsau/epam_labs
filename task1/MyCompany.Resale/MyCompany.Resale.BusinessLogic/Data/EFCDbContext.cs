using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyCompany.Resale.BusinessLogic.Description;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class EFCDbContext : DbContext
    {
       // public const string ConnectionString = @"Server=(localdb)\mssqllocaldb;Database = ResaleDB_Mikalai_Rumiantsau;Trusted_Connection=True;";
        public DbSet<City> Cities { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<PasswordHash> Passwords { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserClaim> Claims { get; set; }
        public DbSet<Venue> Venues { get; set; }

        public EFCDbContext()
        {

        }
        /// <summary>
        /// Constructor for testing
        /// </summary>
        /// <param name="contextOptions"></param>
        public EFCDbContext(DbContextOptions<EFCDbContext> contextOptions)
            :base(contextOptions)
        {
        }
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer();
            }
        }
        */
    }
}