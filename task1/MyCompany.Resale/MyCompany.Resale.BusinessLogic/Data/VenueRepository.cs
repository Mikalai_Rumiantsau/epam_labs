using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class VenueRepository : IVenueRepository
    {
        private readonly EFCDbContext _context;

        public VenueRepository(EFCDbContext context)
        {
            _context = context;
        }

        public Result<IEnumerable<Venue>> Venues
        {
            get
            {
                try
                {
                    return Result.Ok<IEnumerable<Venue>>(_context.Venues.Include(v => v.City));
                }
                catch (DbUpdateException)
                {
                    return Result.Fail<IEnumerable<Venue>>("Unable to open the DB connection");
                }
            }
        }

        public Result Add(Venue venue)
        {
            try
            {
                _context.Venues.Add(venue);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException)
            {
                return Result.Fail("Unable to open the DB connection");
            }
        }

        public Result Delte(Venue venue)
        {
            try
            {
                _context.Venues.Remove(venue);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public Result Update(Venue venue)
        {
            try
            {
                _context.Venues.Update(venue);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail(ex.Message);
            }
        }
    }
}