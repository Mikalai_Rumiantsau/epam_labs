using System.Collections.Generic;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class PasswordRepository : IPasswordRepository
    {
        private readonly EFCDbContext _context;

        public PasswordRepository(EFCDbContext context)
        {
            _context = context;
        }

        public Result<IEnumerable<PasswordHash>> Passwords
        {
            get
            {
                try
                {
                    return Result.Ok<IEnumerable<PasswordHash>>(_context.Passwords);
                }
                catch (DbUpdateException)
                {
                    return Result.Fail<IEnumerable<PasswordHash>>("Unable to open the DB connection");
                }
            }
        }
    }
}