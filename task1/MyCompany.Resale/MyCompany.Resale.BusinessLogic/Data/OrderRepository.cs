using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class OrderRepository : IOrderRepository
    {
        private readonly EFCDbContext _context;

        public OrderRepository(EFCDbContext context)
        {
            _context = context;
        }

        public Result<IEnumerable<Order>> Orders
        {
            get
            {
                try
                {
                    return Result.Ok<IEnumerable<Order>>(
                        _context.Orders
                            .Include(b => b.Buyer)
                            .Include(t => t.Ticket)
                            .ThenInclude(e => e.Event)
                            .Include(t => t.Ticket)
                            .ThenInclude(s => s.Seller));
                }
                catch (DbUpdateException)
                {
                    return Result.Fail<IEnumerable<Order>>("Unable to open the DB connection");
                }
            }

            set { throw new NotImplementedException(); }
        }

        public Result Add(Order order)
        {
            try
            {
                _context.Orders.Add(order);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (Exception)
            {
                return Result.Fail("Unable to open the DB connection");
            }
        }

        public Result Update(Order ordere)
        {
            try
            {
                _context.Orders.Update(ordere);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException)
            {
                return Result.Fail("Unable to open the DB connection");
            }
        }
    }
}