using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using CSharpFunctionalExtensions;
using System;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class TicketRepository : ITicketRepository
    {
        private readonly EFCDbContext _context;

        public TicketRepository(EFCDbContext context)
        {
            _context = context;
        }

        public Result<IEnumerable<Ticket>> Tickets
        {
            get
            {
                try
                {
                    return Result.Ok<IEnumerable<Ticket>>(
                        _context.Tickets
                            .Include(t => t.Event)
                            .ThenInclude(e => e.Venue)
                            .ThenInclude(c => c.City)
                            .Include(s => s.Seller));
                }
                catch (DbUpdateException)
                {
                    return Result.Fail<IEnumerable<Ticket>>("Unable to open the DB connection");
                }
            }
        }

        public Result Add(Ticket ticket)
        {
            try
            {
                _context.Tickets.Add(ticket);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (Exception)
            {
                return Result.Fail("Unable to open the DB connection");
            }
        }

        public Result Update(Ticket ticket)
        {
            try
            {
                _context.Tickets.Update(ticket);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException)
            {
                return Result.Fail("Unable to open the DB connection");
            }
        }

        public Result Delete(Ticket ticket)
        {
            try
            {
                _context.Tickets.Remove(ticket);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail(ex.Message);
            }
        }

        public DbContext Context
        {
            get { return _context; }
        }
    }
}