using System.Collections.Generic;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class ClaimRepository : IClaimRepository
    {
        private readonly EFCDbContext _context;

        public ClaimRepository(EFCDbContext context)
        {
            _context = context;
        }

        public Result<IEnumerable<UserClaim>> Claims
        {
            get
            {
                try
                {
                    return Result.Ok<IEnumerable<UserClaim>>(_context.Claims);
                }
                catch (DbUpdateException)
                {
                    return Result.Fail<IEnumerable<UserClaim>>("Unable to open the DB connection");
                }
            }
        }

        public Result Add(User user, IEnumerable<Claim> claims)
        {
            foreach (var claim in claims)
            {
                _context.Add(new UserClaim
                {
                    ClaimType = claim.Type,
                    ClailValue = claim.Value,
                    User = user,
                    UserId = user.Id
                });
            }
            try
            {
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException)
            {
                return Result.Fail("Unable to open the DB connection");
            }
        }

        public Result Update(UserClaim claim)
        {
            try
            {
                _context.Claims.Update(claim);
                _context.SaveChanges();
                return Result.Ok();
            }
            catch (DbUpdateException ex)
            {
                return Result.Fail(ex.Message);
            }
        }
    }
}