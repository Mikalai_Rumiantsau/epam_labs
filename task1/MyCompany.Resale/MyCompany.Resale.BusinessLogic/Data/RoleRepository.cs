using System.Collections.Generic;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;

namespace MyCompany.Resale.BusinessLogic.Data
{
    public class RoleRepository : IRoleRepository
    {
        private readonly EFCDbContext _context;

        public RoleRepository(EFCDbContext context)
        {
            _context = context;
        }

        public Result<IEnumerable<Role>> Roles
        {
            get
            {
                try
                {
                    return Result.Ok<IEnumerable<Role>>(_context.Roles);
                }
                catch (DbUpdateException)
                {
                    return Result.Fail<IEnumerable<Role>>("Unable to open the DB connection");
                }
            }
        }
    }
}

