﻿using System;
using System.Globalization;

namespace MyCompany.Resale.BusinessLogic.Helpers
{
    public class DateConverter
    {
        public DateTime GetDate(string dateInString)
        {
            DateTime dt = DateTime.ParseExact(dateInString, "yyyy/MM/dd", CultureInfo.InvariantCulture);

            return dt;
        }
    }
}
