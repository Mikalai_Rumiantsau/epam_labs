﻿using System;

namespace MyCompany.Resale.BusinessLogic.Helpers
{
    public class ActualChecker
    {
        public static bool Check(DateTime date)
        {
            return (date >= DateTime.Now);
        }
    }
}
