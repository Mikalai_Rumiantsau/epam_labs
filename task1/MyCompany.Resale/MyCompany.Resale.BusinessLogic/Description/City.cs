﻿using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Description
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Venue> Venues { get; set; }
    }
}
