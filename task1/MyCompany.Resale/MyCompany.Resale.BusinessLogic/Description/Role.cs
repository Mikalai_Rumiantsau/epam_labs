﻿using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Description
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
