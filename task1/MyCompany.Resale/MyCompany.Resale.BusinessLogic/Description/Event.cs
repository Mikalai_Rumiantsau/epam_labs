﻿using System;

namespace MyCompany.Resale.BusinessLogic.Description
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }
        public EventStatus Status { get; set; }
        public Venue Venue { get; set; }
    }
}
