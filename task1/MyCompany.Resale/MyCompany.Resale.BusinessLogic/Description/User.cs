﻿using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Description
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Address { get; set; }
        public string PhoneNmber { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public PasswordHash Password { get; set; }
        public bool EmailConformed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Role Role { get; set; }
        public List<UserClaim> Claims { get; set; }
        public List<Ticket> Tickets { get; set; }
        public List<Order> Orders { get; set; }
    }
}
