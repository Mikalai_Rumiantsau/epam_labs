﻿namespace MyCompany.Resale.BusinessLogic.Description
{
    public enum OrderStatus
    {
        Waiting,
        Confirmed,
        Rejected,
        Complete
    }
}
