﻿namespace MyCompany.Resale.BusinessLogic.Description
{
    public enum TicketStatus
    {
        Selling,
        WaitingConfirmation,
        ConfirmedAndDelivery,
        Delivery,
        Sold
    }
}
