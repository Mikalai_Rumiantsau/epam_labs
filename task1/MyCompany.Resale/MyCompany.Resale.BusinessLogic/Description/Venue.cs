﻿using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Description
{
    public class Venue
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public City City { get; set; }
        public List<Event> Events { get; set; }
    }
}
