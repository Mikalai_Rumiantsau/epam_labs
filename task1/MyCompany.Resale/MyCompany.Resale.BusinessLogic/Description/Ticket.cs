﻿namespace MyCompany.Resale.BusinessLogic.Description
{
    public class Ticket
    {
        public int Id { get; set; }
        public int Price { get; set; }
        public int Number { get; set; }
        public Event Event { get; set; }
        public User Seller { get; set; }
        public TicketStatus Status { get; set; }
        public string Note { get; set; }
    }
}
