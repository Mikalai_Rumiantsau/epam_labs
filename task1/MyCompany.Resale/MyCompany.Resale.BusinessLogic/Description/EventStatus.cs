﻿namespace MyCompany.Resale.BusinessLogic.Description
{
    public enum EventStatus
    {
        Activ,
        Cancelled,
        Postponed
    }
}
