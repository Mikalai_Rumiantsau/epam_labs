﻿namespace MyCompany.Resale.BusinessLogic.Description
{
    public class Order
    {
        public int Id { get; set; }
        public string TrackNumber { get; set; }
        public Ticket Ticket { get; set; }
        public OrderStatus Status { get; set; }
        public User Buyer { get; set; }
        public string Note { get; set; }
    }
}
