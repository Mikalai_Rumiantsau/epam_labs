﻿namespace MyCompany.Resale.BusinessLogic.Description
{
    public class PasswordHash
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
