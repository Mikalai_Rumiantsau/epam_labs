﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Interface
{
    public interface IOrderRepository
    {
        Result<IEnumerable<Order>> Orders { get; set; }
        Result Add(Order order);
        Result Update(Order ordere);
    }
}
