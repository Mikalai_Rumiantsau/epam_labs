﻿using System.Collections.Generic;
using MyCompany.Resale.BusinessLogic.Description;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Interface
{
    public class IPasswordRepository
    {
        Result<IEnumerable<PasswordHash>> Passwords { get; }
    }
}
