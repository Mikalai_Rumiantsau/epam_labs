﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Interface
{
    public interface IEventRepository
    {
        Result<IEnumerable<Event>> Events { get; }
        Result Add(Event newEvent);
        Result Update(Event ev);
        Result Delete(Event ev);
    }
}
