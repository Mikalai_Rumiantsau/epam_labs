﻿using System.Collections.Generic;
using MyCompany.Resale.BusinessLogic.Description;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Interface
{
    public interface IRoleRepository
    {
        Result<IEnumerable<Role>> Roles { get; }
    }
}
