﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using CSharpFunctionalExtensions;
using System.Security.Claims;

namespace MyCompany.Resale.BusinessLogic.Interface
{
    public interface IClaimRepository
    {
        Result<IEnumerable<UserClaim>> Claims { get; }
        Result Add(User user, IEnumerable<Claim> claims);
        Result Update(UserClaim claim);
    }
}
