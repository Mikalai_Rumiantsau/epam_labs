﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Interface
{
    public interface ICityRepository
    {
        Result<IEnumerable<City>> Cities { get; }
        Result Delte(City city);
        Result Add(City city);
        Result Update(City city);
    }
}
