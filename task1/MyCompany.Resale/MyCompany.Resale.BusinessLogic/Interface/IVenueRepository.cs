﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Interface
{
    public interface IVenueRepository
    {
        Result<IEnumerable<Venue>> Venues { get; }
        Result Add(Venue venue);
        Result Delte(Venue venue);
        Result Update(Venue venue);
    }
}
