﻿using System.Collections.Generic;
using MyCompany.Resale.BusinessLogic.Description;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;

namespace MyCompany.Resale.BusinessLogic.Interface
{
    public interface IUserRepository
    {
        Result<IEnumerable<User>> Users { get; }
        Result Add(User user);
        Result Update(User user);
        DbContext Context { get; }
    }
}
