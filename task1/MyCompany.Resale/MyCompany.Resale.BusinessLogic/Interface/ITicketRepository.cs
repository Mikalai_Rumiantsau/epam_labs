﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using CSharpFunctionalExtensions;
using Microsoft.EntityFrameworkCore;

namespace MyCompany.Resale.BusinessLogic.Interface
{
    public interface ITicketRepository
    {
        Result<IEnumerable<Ticket>> Tickets { get; }
        Result Add(Ticket ticket);
        Result Update(Ticket ticket);
        Result Delete(Ticket ticket);
        DbContext Context { get; }
    }
}
