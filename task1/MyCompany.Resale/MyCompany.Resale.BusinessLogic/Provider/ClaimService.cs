using System.Collections.Generic;
using System.Linq;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Provider
{
    public class ClaimService : IClaimService
    {
        private readonly IClaimRepository _claimRepository;
        public ClaimService(IClaimRepository claimRepository)
        {
            _claimRepository = claimRepository;
        }
        public Result<List<UserClaim>> GetClailmsByUserId(int userId)
        {
            var claims = _claimRepository.Claims;

            if (claims.IsSuccess)
            {
                var userClaims = claims.Value.Where(c => c.UserId == userId).ToList();
                if (userClaims != null)
                    return Result.Ok(userClaims);

                return Result.Fail<List<UserClaim>>("Such claims isn't present in DB.");
            }
            return Result.Fail<List<UserClaim>>(claims.Error);
        }
        public Result UpdateClaim(UserClaim claim)
        {
            return _claimRepository.Update(claim);
        }
    }
}