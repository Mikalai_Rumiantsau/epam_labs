﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using System.Linq;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Provider
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Result<List<Order>> GetOrdersByUserId(int id)
        {
            var ordersRes = _orderRepository.Orders;
            if (ordersRes.IsSuccess)
            {
                var orders = ordersRes.Value.Where(u => u.Buyer.Id == id).ToList();
                if (orders != null)
                    return Result.Ok(orders);
                return Result.Fail<List<Order>>("This user haven't orders");
            }
            return Result.Fail<List<Order>>(ordersRes.Error);
        }
        public Result AddOrder(Ticket ticket, User user)
        {
            var order = new Order
            {
                Ticket = ticket,
                Status = OrderStatus.Waiting,
                Buyer = user
            };
            return _orderRepository.Add(order);
        }
        public Result<Order> GetOrderByTicketId(int ticketId)
        {
            var orderRes = _orderRepository.Orders;
            if (orderRes.IsSuccess)
            {
                var order = orderRes.Value.FirstOrDefault(o => o.Ticket.Id == ticketId);
                if (order != null)
                    return Result.Ok(order);
                return Result.Fail<Order>("Such order isn't present in DB.");
            }
            return Result.Fail<Order>(orderRes.Error);
        }
        public Result UpdateOrder(Order ordere)
        {
            return _orderRepository.Update(ordere);
        }
        public Result<Order> GetOrderById(int orderId)
        {
            var orderRes = _orderRepository.Orders;
            if (orderRes.IsSuccess)
            {
                var order = orderRes.Value.FirstOrDefault(o => o.Id == orderId);
                if (order != null)
                    return Result.Ok(order);
                return Result.Fail<Order>("Such order isn't present in DB.");
            }
            return Result.Fail<Order>(orderRes.Error);
        }
        public Result<Order> SetStatus(Order order, OrderStatus status)
        {
            if (order != null)
            {
                order.Status = status;
                return Result.Ok(order);
            }
            return Result.Fail<Order>("Order is Null");
        }
        public Result<Order> SetNote(Order order, string note)
        {
            if (order != null)
            {
                if (!string.IsNullOrWhiteSpace(note))
                {
                    order.Note = note;
                    return Result.Ok(order);
                }
                return Result.Fail<Order>("Note field is empty");
            }
            return Result.Fail<Order>("Order is Null");
        }
        public Result<Order> SetTrackNumber(Order order, string trackNumber)
        {
            if (order != null)
            {
                if (!string.IsNullOrWhiteSpace(trackNumber))
                {
                    order.TrackNumber = trackNumber;
                    return Result.Ok(order);
                }
                return Result.Fail<Order>("Tracknumber field is empty");
            }
            return Result.Fail<Order>("Order is Null or Tracknumber is empty");
        }
    }
}
