﻿using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using System;
using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Provider.Interface
{
    public interface IEventService
    {
        Result<List<Event>> GetAllEvents();
        Result<List<Event>> GetAllActualEvents();
        Result<List<City>> GetAllCity();
        Result<List<Venue>> GetAllVenue();
        Result<Venue> GetVenueById(int id);
        Result AddNewEvent(Event newEvent);
        Result<Event> GetEventById(int id);
        Result UpdateEvent(Event ev);
        Result DeleteEvent(Event ev);

        Result<Event> CreateEvent(string name, Venue venue, DateTime dateStart, DateTime dateEnd, string banner,
            string description);

    }
}
