﻿using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Provider.Interface
{
    public interface IOrderService
    {
        Result<List<Order>> GetOrdersByUserId(int id);
        Result AddOrder(Ticket ticket, User user);
        Result<Order> GetOrderByTicketId(int ticketId);
        Result UpdateOrder(Order ordere);
        Result<Order> GetOrderById(int orderId);
        Result<Order> SetStatus(Order order, OrderStatus status);
        Result<Order> SetNote(Order order, string note);
        Result<Order> SetTrackNumber(Order order, string trackNumber);
    }
}
