﻿using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Provider.Interface
{
    public interface ICityService
    {
        Result<List<City>> GetAllCities();
        Result<City> GetCityById(int cityId);
        Result<City> GetCityByName(string cityName);
        Result DeleteCity(City city);
        Result UpdateCity(City city);
        Result AddCity(City city);
    }
}
