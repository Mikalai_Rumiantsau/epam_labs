﻿using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Provider.Interface
{
    public interface IVenueService
    {
        Result<List<Venue>> GetAllVenues();
        Result<Venue> GetVenueById(int venueId);
        Result<Venue> GetVenueByName(string venueName);
        Result DeleteVenue(Venue venue);
        Result UpdateVenue(Venue venue);
        Result AddVenue(Venue venue);
    }
}
