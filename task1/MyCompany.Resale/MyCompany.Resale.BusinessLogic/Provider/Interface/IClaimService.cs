using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Provider.Interface
{
    public interface IClaimService
    {
        Result<List<UserClaim>> GetClailmsByUserId(int userId);
        Result UpdateClaim(UserClaim claim);
    }
}