﻿using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Provider.Interface
{
    public interface ITicketService
    {
        Result<List<Ticket>> GetAllTickets();
        Result<List<Ticket>> GetTicketsByEevent(int eventId);
        Result<List<Ticket>> GetTicketsByUser(User user);
        Result<Ticket> GetTicketById(int id);
        Result<List<Ticket>> GetTicketsByStatus(TicketStatus status);
        Result<List<Ticket>> GetTicketsByStatus(TicketStatus status, int userId);
        Result AddNewTicket(Ticket ticket);
        Result UpdateTicket(Ticket ticket);
        Result Delete(Ticket ticket);
        Result<Ticket> SetStatus(Ticket ticket, TicketStatus status);
        Result TransactionUpdateTicket(Ticket ticket, Order order);
    }
}
