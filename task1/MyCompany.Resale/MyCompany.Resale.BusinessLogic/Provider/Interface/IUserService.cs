﻿using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;

namespace MyCompany.Resale.BusinessLogic.Provider.Interface
{
    public interface IUserService
    {
        Result<List<User>> GetAllUsers();
        Result<List<Role>> GetAllRole();
        Result UpdateUserRole(User user, Role role);
    }
}
