﻿using MyCompany.Resale.BusinessLogic.Description;
using System.Collections.Generic;
using System.Linq;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using MyCompany.Resale.BusinessLogic.Data;
using CSharpFunctionalExtensions;
using System;

namespace MyCompany.Resale.BusinessLogic.Provider
{
    public class EventService : IEventService
    {
        private readonly IEventRepository _eventRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IVenueRepository _venueRepository;

        public EventService(IEventRepository eventRepository, ICityRepository cityRepository,
            IVenueRepository venueRepository)
        {
            _eventRepository = eventRepository;
            _cityRepository = cityRepository;
            _venueRepository = venueRepository;
        }

        public Result<List<Event>> GetAllEvents()
        {
            var events = _eventRepository.Events;
            if (events.IsSuccess)
            {
                return Result.Ok(events.Value.ToList());
            }
            return Result.Fail<List<Event>>(events.Error);
        }

        public Result<List<Event>> GetAllActualEvents()
        {
            var events = _eventRepository.Events;
            if (events.IsSuccess)
            {
                return Result.Ok(events.Value.Where(e => e.DateEnd >= DateTime.Now).ToList());
            }
            return Result.Fail<List<Event>>(events.Error);
        }
        public Result<List<City>> GetAllCity()
        {
            var cities = _cityRepository.Cities;

            if (cities.IsSuccess)
            {
                return Result.Ok(cities.Value.ToList());
            }

            return Result.Fail<List<City>>(cities.Error);
        }
        public Result<List<Venue>> GetAllVenue()
        {
            var venues = _venueRepository.Venues;

            if (venues.IsSuccess)
            {
                return Result.Ok(venues.Value.ToList());
            }

            return Result.Fail<List<Venue>>(venues.Error);
        }
        public Result<Venue> GetVenueById(int id)
        {
            var venes = _venueRepository.Venues;

            if (venes.IsSuccess)
            {
                var venue = venes.Value.FirstOrDefault(v => v.Id == id);

                if (venue != null)
                    return Result.Ok(venue);
                return Result.Fail<Venue>("Such venue isn't present in DB.");
            }
            return Result.Fail<Venue>(venes.Error);
        }
        public Result AddNewEvent(Event newEvent)
        {
            var events = _eventRepository.Events;

            if (events.IsSuccess)
            {
                var ev = events.Value
                    .Where(e => e.Name == newEvent.Name)
                    .FirstOrDefault(e => e.DateStart == newEvent.DateStart);

                if (ev == null)
                    return _eventRepository.Add(newEvent);

                return Result.Fail("We have a same event in DB");
            }
            return Result.Fail(events.Error);
        }
        public Result<Event> GetEventById(int id)
        {
            var events = _eventRepository.Events;

            if (events.IsSuccess)
            {
                var ev = events.Value.FirstOrDefault(e => e.Id == id);
                if (ev != null)
                    return Result.Ok(ev);
                return Result.Fail<Event>("Such event isn't present in DB.");
            }
            return Result.Fail<Event>(events.Error);
        }
        public Result<Event> CreateEvent(string name, Venue venue, DateTime dateStart, DateTime dateEnd, string banner,
            string description)
        {
            return Result.Ok(new Event
            {
                Name = name,
                Venue = venue,
                DateStart = dateStart,
                DateEnd = dateEnd,
                Banner = banner,
                Status = EventStatus.Activ,
                Description = description
            });
        }
        public Result UpdateEvent(Event ev)
        {
            return _eventRepository.Update(ev);
        }
        public Result DeleteEvent(Event ev)
        {
            return _eventRepository.Delete(ev);
        }
    }
}
