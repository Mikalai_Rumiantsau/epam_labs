﻿using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCompany.Resale.BusinessLogic.Provider
{
    public class VenueService : IVenueService
    {
        private readonly IVenueRepository _venueRepository;
        public VenueService(IVenueRepository venueRepository)
        {
            _venueRepository = venueRepository;
        }

        public Result<List<Venue>> GetAllVenues()
        {
            var venues = _venueRepository.Venues;
            if (venues.IsFailure)
                return Result.Fail<List<Venue>>(venues.Error);
            return Result.Ok(venues.Value.ToList());
        }

        public Result<Venue> GetVenueById(int venueId)
        {
            var venues = _venueRepository.Venues;
            if (venues.IsFailure)
                return Result.Fail<Venue>(venues.Error);
            var venue = venues.Value.FirstOrDefault(v => v.Id == venueId);
            if (venue == null)
                return Result.Fail<Venue>("We haven't this venue in repository");
            return Result.Ok(venue);
        }

        public Result<Venue> GetVenueByName(string venueName)
        {
            var venues = _venueRepository.Venues;
            if (venues.IsFailure)
                return Result.Fail<Venue>(venues.Error);
            var venue = venues.Value.FirstOrDefault(c => c.Name == venueName);
            if (venue == null)
                return Result.Fail<Venue>("We haven't this venue in repository");
            return Result.Ok(venue);
        }

        public Result DeleteVenue(Venue venue)
        {
            var deleteResult = _venueRepository.Venues
                .OnSuccess(c => _venueRepository.Delte(venue));
            if (deleteResult.IsFailure)
                return Result.Fail<Venue>(deleteResult.Error);
            return Result.Ok();
        }

        public Result UpdateVenue(Venue venue)
        {
            var updateResult = _venueRepository.Venues
                .OnSuccess(c => _venueRepository.Update(venue));
            if (updateResult.IsFailure)
               return Result.Fail<Venue>(updateResult.Error);
            return Result.Ok();
        }

        public Result AddVenue(Venue venue)
        {
            var addResult = _venueRepository.Venues
                .OnSuccess(v => _venueRepository.Add(venue));
            if (addResult.IsFailure)
                return Result.Fail<Venue>(addResult.Error);
            return Result.Ok();
        }
    }
}
