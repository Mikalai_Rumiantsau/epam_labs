﻿using MyCompany.Resale.BusinessLogic.Description;
using System;
using System.Collections.Generic;
using System.Linq;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using CSharpFunctionalExtensions;

namespace MyCompany.Resale.BusinessLogic.Provider
{
    public class TicketService : ITicketService
    {
        private readonly ITicketRepository _ticketRepository;
        private readonly IOrderRepository _orderRepository;

        public TicketService(ITicketRepository ticketRepository, IOrderRepository orderRepository)
        {
            _ticketRepository = ticketRepository;
            _orderRepository = orderRepository;
        }

        public Result<List<Ticket>> GetAllTickets()
        {
            var ticketsRes = _ticketRepository.Tickets;
            if (ticketsRes.IsSuccess)
            {
                var tickets = ticketsRes.Value.ToList();
                if (tickets != null)
                    return Result.Ok(tickets);
                return Result.Fail<List<Ticket>>("We haven't any event in DB");
            }
            return Result.Fail<List<Ticket>>(ticketsRes.Error);
        }

        public Result<Ticket> GetTicketById(int id)
        {
            var ticketsRes = _ticketRepository.Tickets;
            if (ticketsRes.IsSuccess)
            {
                var ticket = ticketsRes.Value.FirstOrDefault(x => x.Id == id);
                if (ticket != null)
                    return Result.Ok(ticket);
                return Result.Fail<Ticket>("Such ticket isn't present in DB.");
            }
            return Result.Fail<Ticket>(ticketsRes.Error);
        }


        public Result<List<Ticket>> GetTicketsByEevent(int eventId)
        {
            var ticketsRes = _ticketRepository.Tickets;
            if (ticketsRes.IsSuccess)
            {
                var tickets = ticketsRes.Value.Where(x => x.Event.Id == eventId)
                    .Where(t => t.Status == TicketStatus.Selling)
                    .ToList();
                if (tickets != null)
                    return Result.Ok(tickets);
                return Result.Fail<List<Ticket>>("Such tickets isn't present in DB.");
            }
            return Result.Fail<List<Ticket>>(ticketsRes.Error);
        }


        public Result<List<Ticket>> GetTicketsByStatus(TicketStatus status)
        {
            var ticketsRes = _ticketRepository.Tickets;
            if (ticketsRes.IsSuccess)
            {
                var tickets = ticketsRes.Value.Where(t => t.Status == status).ToList();
                if (tickets != null)
                    return Result.Ok(tickets);
                return Result.Fail<List<Ticket>>("Such tickets isn't present in DB.");
            }
            return Result.Fail<List<Ticket>>(ticketsRes.Error);
        }

        public Result<List<Ticket>> GetTicketsByStatus(TicketStatus status, int userId)
        {
            var ticketsRes = _ticketRepository.Tickets;
            if (ticketsRes.IsSuccess)
            {
                var tickets = ticketsRes.Value.Where(t => t.Status == status)
                    .Where(t => t.Seller.Id == userId)
                    .ToList();
                if (tickets != null)
                    return Result.Ok(tickets);
                return Result.Fail<List<Ticket>>("Such tickets isn't present in DB.");
            }

            return Result.Fail<List<Ticket>>(ticketsRes.Error);
        }


        public Result AddNewTicket(Ticket ticket)
        {
            var ticketsRes = _ticketRepository.Tickets;
            if (ticketsRes.IsSuccess)
            {
                var tempTicket = ticketsRes.Value
                    .Where(t => t.Event.Name == ticket.Event.Name)
                    .FirstOrDefault(t => t.Number == ticket.Number);

                if (tempTicket == null)
                    return _ticketRepository.Add(ticket);
                return Result.Fail("Such ticket already exist in DB.");
            }

            return Result.Fail(ticketsRes.Error);
        }

        public Result<Ticket> SetStatus(Ticket ticket, TicketStatus status)
        {
            if (ticket != null)
            {
                ticket.Status = status;
                return Result.Ok(ticket);
            }
            return Result.Fail<Ticket>("Ticket is Null");
        }

        public Result<List<Ticket>> GetTicketsByUser(User user)
        {
            var ticketsRes = _ticketRepository.Tickets;
            if (ticketsRes.IsFailure)
                return Result.Fail<List<Ticket>>(ticketsRes.Error);
            var tickets = ticketsRes.Value
                .Where(t => t.Seller.Id == user.Id)
                .ToList();
            if (tickets != null)
                return Result.Ok(tickets);
            return Result.Fail<List<Ticket>>("This user haven't tickets.");
        }
        public Result UpdateTicket(Ticket ticket)
        {
            return _ticketRepository.Update(ticket);
        }
        public Result Delete(Ticket ticket)
        {
            return _ticketRepository.Delete(ticket);
        }
        public Result TransactionUpdateTicket(Ticket ticket, Order order)
        {
            using (var tr = _ticketRepository.Context.Database.BeginTransaction())
            {
                try
                {
                    _orderRepository.Update(order);
                    _ticketRepository.Update(ticket);
                    tr.Commit();
                    return Result.Ok();
                }
                catch (Exception e)
                {
                    tr.Rollback();
                    return Result.Fail(e.Message);
                }
            }
        }
    }
}
