﻿using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using System.Collections.Generic;
using System.Linq;

namespace MyCompany.Resale.BusinessLogic.Provider
{
    public class CityService : ICityService
    {
        private readonly ICityRepository _cityRepository;

        public CityService(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        public Result<List<City>> GetAllCities()
        {
            var cities = _cityRepository.Cities;
            if (cities.IsFailure)
                return Result.Fail<List<City>>(cities.Error);

            return Result.Ok(cities.Value.ToList());
        }

        public Result<City> GetCityById(int cityId)
        {
            var cities = _cityRepository.Cities;
            if (cities.IsFailure)
                return Result.Fail<City>(cities.Error);

            var city = cities.Value.FirstOrDefault(c => c.Id == cityId);
            if (city == null)
                return Result.Fail<City>("We haven't this city in repository");

            return Result.Ok(city);
        }

        public Result<City> GetCityByName(string cityName)
        {
            var cities = _cityRepository.Cities;
            if (cities.IsFailure)
                return Result.Fail<City>(cities.Error);

            var city = cities.Value.FirstOrDefault(c => c.Name == cityName);
            if (city == null)
                return Result.Fail<City>("We haven't this city in repository");

            return Result.Ok(city);
        }

        public Result DeleteCity(City city)
        {
            var deleteResult = _cityRepository.Cities
                .OnSuccess(c => _cityRepository.Delte(city));

            if (deleteResult.IsFailure)
                return Result.Fail<City>(deleteResult.Error);

            return Result.Ok();
        }

        public Result UpdateCity(City city)
        {
            var updateResult = _cityRepository.Cities
                .OnSuccess(c => _cityRepository.Update(city));

            if (updateResult.IsFailure)
                return Result.Fail<City>(updateResult.Error);

            return Result.Ok();
        }

        public Result AddCity(City city)
        {
            var addResult = _cityRepository.Cities
                .OnSuccess(c => _cityRepository.Add(city));

            if (addResult.IsFailure)
                return Result.Fail<City>(addResult.Error);

            return Result.Ok();
        }
    }
}
