﻿using CSharpFunctionalExtensions;
using MyCompany.Resale.BusinessLogic.Description;
using MyCompany.Resale.BusinessLogic.Interface;
using MyCompany.Resale.BusinessLogic.Provider.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace MyCompany.Resale.BusinessLogic.Provider
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IClaimRepository _claimRepository;

        public UserService(IUserRepository userRepository, IRoleRepository roleRepository,
            IClaimRepository claimRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _claimRepository = claimRepository;
        }

        public Result<List<User>> GetAllUsers()
        {
            var users = _userRepository.Users;
            if (users.IsFailure)
                return Result.Fail<List<User>>(users.Error);
            return Result.Ok(users.Value.ToList());
        }

        public Result<List<Role>> GetAllRole()
        {
            var roles = _roleRepository.Roles;
            if (roles.IsFailure)
                return Result.Fail<List<Role>>(roles.Error);
            return Result.Ok(roles.Value.ToList());
        }

        public Result UpdateUserRole(User user, Role role)
        {
            var claims = _claimRepository.Claims;
            if (claims.IsFailure)
                return Result.Fail(claims.Error);

            using (var tr = _userRepository.Context.Database.BeginTransaction())
            {
                var claim = claims.Value.Where(c => c.ClaimType == ClaimTypes.Role)
                    .FirstOrDefault(c => c.UserId == user.Id);

                user.Role = role;
                claim.ClailValue = role.Name;

                var userUpdateResult = _userRepository.Update(user);
                var claimUpdateResult = _claimRepository.Update(claim);

                var commonResult = Result.Combine(userUpdateResult, claimUpdateResult);
                if (commonResult.IsFailure)
                {
                    tr.Rollback();
                    return Result.Fail(commonResult.Error);
                }
                tr.Commit();
                return Result.Ok();
            }
        }
    }
}
