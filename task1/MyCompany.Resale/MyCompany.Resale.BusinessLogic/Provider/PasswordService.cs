using System.Security.Cryptography;
using System.Text;

namespace MyCompany.Resale.BusinessLogic.Provider
{
    public class PasswordService
    {
        public static string GetMD5Hash(string passwordString)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(passwordString);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}