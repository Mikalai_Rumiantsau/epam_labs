﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using MyCompany.Resale.BusinessLogic.Data;
using MyCompany.Resale.BusinessLogic.Description;

namespace MyCompany.Resale.BusinessLogic.Migrations
{
    [DbContext(typeof(EFCDbContext))]
    partial class EFCDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.City", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Cities");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Event", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Banner");

                    b.Property<DateTime>("DateEnd");

                    b.Property<DateTime>("DateStart");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int>("Status");

                    b.Property<int?>("VenueId");

                    b.HasKey("Id");

                    b.HasIndex("VenueId");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BuyerId");

                    b.Property<string>("Note");

                    b.Property<int>("Status");

                    b.Property<int?>("TicketId");

                    b.Property<string>("TrackNumber");

                    b.HasKey("Id");

                    b.HasIndex("BuyerId");

                    b.HasIndex("TicketId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Password", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("UserId");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("UserId")
                        .IsUnique();

                    b.ToTable("Passwords");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Ticket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("EventId");

                    b.Property<string>("Note");

                    b.Property<int>("Number");

                    b.Property<int>("Price");

                    b.Property<int?>("SellerId");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.HasIndex("SellerId");

                    b.ToTable("Tickets");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("Email");

                    b.Property<bool>("EmailConformed");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Localization");

                    b.Property<string>("Login");

                    b.Property<string>("PhoneNmber");

                    b.Property<int?>("RoleId");

                    b.Property<bool>("TwoFactorEnabled");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.UserClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClailValue");

                    b.Property<string>("ClaimType");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Claims");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Venue", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int?>("CityId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CityId");

                    b.ToTable("Venues");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Event", b =>
                {
                    b.HasOne("MyCompany.Resale.BusinessLogic.Description.Venue", "Venue")
                        .WithMany("Events")
                        .HasForeignKey("VenueId");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Order", b =>
                {
                    b.HasOne("MyCompany.Resale.BusinessLogic.Description.User", "Buyer")
                        .WithMany("Orders")
                        .HasForeignKey("BuyerId");

                    b.HasOne("MyCompany.Resale.BusinessLogic.Description.Ticket", "Ticket")
                        .WithMany()
                        .HasForeignKey("TicketId");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Password", b =>
                {
                    b.HasOne("MyCompany.Resale.BusinessLogic.Description.User", "User")
                        .WithOne("Password")
                        .HasForeignKey("MyCompany.Resale.BusinessLogic.Description.Password", "UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Ticket", b =>
                {
                    b.HasOne("MyCompany.Resale.BusinessLogic.Description.Event", "Event")
                        .WithMany()
                        .HasForeignKey("EventId");

                    b.HasOne("MyCompany.Resale.BusinessLogic.Description.User", "Seller")
                        .WithMany("Tickets")
                        .HasForeignKey("SellerId");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.User", b =>
                {
                    b.HasOne("MyCompany.Resale.BusinessLogic.Description.Role", "Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.UserClaim", b =>
                {
                    b.HasOne("MyCompany.Resale.BusinessLogic.Description.User", "User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MyCompany.Resale.BusinessLogic.Description.Venue", b =>
                {
                    b.HasOne("MyCompany.Resale.BusinessLogic.Description.City", "City")
                        .WithMany("Venues")
                        .HasForeignKey("CityId");
                });
        }
    }
}
