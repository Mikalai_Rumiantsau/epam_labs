﻿using System.Windows;
using WpfApplication3.ViewModels;

namespace WpfApplication3.Views
{
    /// <summary>
    /// Логика взаимодействия для TestWindowView.xaml
    /// </summary>
    public partial class TestWindowView : Window
    {
        public TestWindowView(TestWindowViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
