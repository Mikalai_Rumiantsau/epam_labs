﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfApplication3.Models;
using WpfApplication3.ViewModels;

namespace WpfApplication3.Views
{
    /// <summary>
    /// Логика взаимодействия для TestWindowView.xaml
    /// </summary>
    public partial class TestWindowView : Window
    {
        CancellationTokenSource cts;
        Stopwatch delayTimerForСhart = new Stopwatch();
        int googRequest = 0, errorRequest = 0;
        List<Request> requests = new List<Request>();
        public TestWindowViewModel ViewModel { get; private set; }

        public Settings Model { get; private set; }
        bool progressFlag = true;
        DispatcherTimer dispatcherTimer = new DispatcherTimer();

        PieSeries seriesP1 = new PieSeries();

        Stopwatch operationTime = new Stopwatch();

        public TestWindowView(TestWindowViewModel viewModel, bool flag)
        {
            this.Model = viewModel.Model;
            this.ViewModel = viewModel;
            InitializeComponent();
            DataContext = viewModel;
            seriesP1.InnerDiameter = 0;
            seriesP1.ExplodedDistance = 0.0;
            seriesP1.Stroke = OxyColors.White;
            seriesP1.StrokeThickness = 2.0;
            seriesP1.InsideLabelPosition = 0.8;
            seriesP1.AngleSpan = 360;
            seriesP1.StartAngle = 0;
            seriesP1.Slices.Add(new PieSlice("Successful request", 0) { IsExploded = true });
            seriesP1.Slices.Add(new PieSlice("Failed request", 0));
            ViewModel.Model1.Series.Add(seriesP1);
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            ProgressBar.Value++;
            TextInProgress.Text = String.Format("{0:####}%", ProgressBar.Value*(100*1.0/Model.Duration));
        }

        public async void button1_Click(object sender, RoutedEventArgs e)
        {
            progressFlag = true;
            dispatcherTimer.Stop();
            TextInProgress.Text = string.Empty;
            ProgressBar.Value = 0;
            ProgressBar.Minimum = 0;
            ProgressBar.Maximum = Model.Duration;
            dispatcherTimer.Start();
            requests.Clear();
            button.Visibility = Visibility.Visible;
            textBox.Clear();
            googRequest = 0;
            errorRequest = 0;
            ViewModel.Points.Clear();

            Plot1.Title = "Real time chart!!!";
        
            ViewModel.Model1.Title = "Real time pie chart!!!";

            cts = new CancellationTokenSource();

            Stopwatch delayTimer = new Stopwatch();
            int shiftTime = 0;
            try
            {
                cts.CancelAfter(ConvertToMilliseconds(Model.Duration));
                int pass = 0;
                delayTimer.Start();
                while (delayTimer.Elapsed < TimeSpan.FromMilliseconds(ConvertToMilliseconds(Model.Duration)))
                {
                    delayTimerForСhart.Start();
                    await AccessTheWebAsync(cts.Token, shiftTime);
                    textBox.Text += "\r\nDownloads complete. " + ++pass;
                }
            }
            catch (OperationCanceledException)
            {
                textBox.Text += "\r\nThe test has been completed.\r\n";
                textBox.Text += String.Format("\r\nGoodRequesr = {0}, ErrorRequest = {1} ", googRequest, errorRequest);
                requests.Sort();
                dispatcherTimer.Stop();
                if (progressFlag)
                {
                    ProgressBar.Value = 100;
                    TextInProgress.Text = String.Format("{0:####}% - Test done", 100);
                }
                else
                {
                    TextInProgress.Text = "The test was aborted";
                }
             //   MessageBox.Show(requests[0].ToString()+"                  1111111111111");
             //   MessageBox.Show(requests[requests.Count - 1].ToString());
            }
            catch (Exception msg)
            {
                dispatcherTimer.Stop();
                textBox.Text += "\r\nDownloads failed.\r\n" + msg.Message;
            }
            cts = null;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (cts != null)
            {
                progressFlag = false;
                cts.Cancel();
            }
            dispatcherTimer.Stop();
        }

        private int ConvertToMilliseconds (int seconds)
        {
            return seconds * 1000;
        }

        async Task AccessTheWebAsync(CancellationToken ct, int shiftTime )
        {
            HttpClient client = new HttpClient();
            List<int> taskList = SetUpTaskList(Model.AmountOfThreads);
            IEnumerable<Task<int>> downloadTasksQuery =
               from url in taskList select ProcessURL(Model.URL, client, ct);
            List<Task<int>> downloadTasks = downloadTasksQuery.ToList();
            bool flagTimeOut = false;
            
            while (downloadTasks.Count > 0)
            {
                if (flagTimeOut)
                {
                    await Task.Delay(ConvertToMilliseconds(Model.AmountOfRequests), ct);
                }
                Task<int> firstFinishedTask = await Task.WhenAny(downloadTasks);
                downloadTasks.Remove(firstFinishedTask);
                operationTime.Restart();
                int length = await firstFinishedTask;
                TimeSpan time = operationTime.Elapsed;
                double y = length / 100000.0;

                if (length==0)
                {
                    requests.Add(new Request(time, false));
                    errorRequest++;
                    AddPoint(Convert.ToDouble(System.DateTime.Now.Ticks), y);
                    textBox.Text += String.Format("\r\nDownload error ");
                    EditInfoInPieChart();
                }
                else
                {
                    requests.Add(new Request(time, true));
                    textBox.Text += String.Format("\r\nLength of the download:  {0} id ={1:0.#######} Time: {2}", System.DateTime.Now.Ticks, y, time);
                    googRequest++;
                    AddPoint(Convert.ToDouble(System.DateTime.Now.Ticks), y);
                    EditInfoInPieChart();
                }
                flagTimeOut = true;
            }
        }
        private void EditInfoInPieChart ()
        {
            seriesP1.Title = String.Format("Total request: {0}", googRequest+errorRequest);
            seriesP1.Slices.RemoveAt(0);
            seriesP1.Slices.Insert(0, new PieSlice("Successful request", googRequest) { IsExploded = true });
            seriesP1.Slices.RemoveAt(1);
            seriesP1.Slices.Insert(1, new PieSlice("Failed request", errorRequest) { IsExploded = true });
            Plot2.InvalidatePlot(true);
        }
        private void AddPoint(double x, double y)
        {
            ViewModel.Points.Add(new DataPoint(x, y));
            Plot1.InvalidatePlot(true);
        }


        private List<int> SetUpTaskList(int countTask)
        {
            List<int> taskCount = new List<int>();
            for (int i = 0; i < countTask; i++)
            {
                taskCount.Add(i);
            }
            return taskCount;
        }

        async Task<int> ProcessURL(string url, HttpClient client, CancellationToken ct)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(url, ct);
                byte[] urlContents = await response.Content.ReadAsByteArrayAsync();
                return urlContents.Length;
            }
            catch
            {
                return 0;
            }
        }
    }
}
