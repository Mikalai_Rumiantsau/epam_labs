﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WpfApplication3.Models;
using WpfApplication3.Views;

namespace WpfApplication3.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            this.LoadTestPageCommand = new DelegateCommand(o => this.LoadTestPage());
            this.LoadSettingsPageCommand = new DelegateCommand(o => this.LoadSettingsPage());
            this.ExitCommand = new DelegateCommand(o => this.Exit());
            CurrentViewModel = new SettingsPageViewModel(
                new Settings() { AmountOfThreads = 10, AmountOfRequests = 1, Duration = 120, URL = "https://www.tut.by/" });
        }

        public ICommand LoadTestPageCommand { get; private set; }
        public ICommand LoadSettingsPageCommand { get; private set; }
        public ICommand ExitCommand { get; private set; }

        private ViewModelBase _currentViewModel;

        public ViewModelBase CurrentViewModel
        {
            get { return _currentViewModel; }
            set
            {
                _currentViewModel = value;
                this.OnPropertyChanged("CurrentViewModel");
            }
        }

        private void Exit()
        {
            Application.Current.Shutdown();
        }

        public void LoadTestPage()
        {
            TestWindowView testWindow = new TestWindowView(new TestWindowViewModel(
                new Settings() { AmountOfThreads = 10, AmountOfRequests = 1, Duration = 120, URL = "https://www.tut.by/" }, true));
            testWindow.ShowDialog();
        }

        private void LoadSettingsPage()
        {
            CurrentViewModel = new SettingsPageViewModel(
                new Settings() { AmountOfThreads = 20, AmountOfRequests = 1, Duration = 120, URL = "https://www.tut.by/" });
        }
    }
}
