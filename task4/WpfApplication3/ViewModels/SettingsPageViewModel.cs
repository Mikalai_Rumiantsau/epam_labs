﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WpfApplication3.Models;
using WpfApplication3.Views;

namespace WpfApplication3.ViewModels
{
    public class SettingsPageViewModel : ViewModelBase
    {
        public SettingsPageViewModel(Settings model)
        {
            this.Model = model;
            this.LoadTestPageCommand = new DelegateCommand(o => this.LoadTestPage());
        }

        public ICommand LoadTestPageCommand { get; private set; }
        public Settings Model { get; private set; }

        private void LoadTestPage()
        {
           TestWindowView testWindow = new TestWindowView(new TestWindowViewModel(Model, false));
            testWindow.ShowDialog();
        }

        public string URL
        {
            get
            {
                return this.Model.URL;
            }
            set
            {
                this.Model.URL = value;
                this.OnPropertyChanged("URL");
            }
        }

        public int AmountOfThreads
        {
            get
            {
                return this.Model.AmountOfThreads;
            }
            set
            {
                this.Model.AmountOfThreads = value;
                this.OnPropertyChanged("AmountOfThreads");
            }
        }

        public int AmountOfRequests
        {
            get
            {
                return this.Model.AmountOfRequests;
            }
            set
            {
                this.Model.AmountOfRequests = value;
                this.OnPropertyChanged("AmountOfRequests");
            }
        }

        public int Duration
        {
            get
            {
                return this.Model.Duration;
            }
            set
            {
                this.Model.Duration = value;
                this.OnPropertyChanged("Duration");
            }
        }
    }
}
