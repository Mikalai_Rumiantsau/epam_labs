﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using WpfApplication3.Models;

namespace WpfApplication3.ViewModels
{
    public class TestWindowViewModel : ViewModelBase
    {
        CancellationTokenSource cts;
        Stopwatch delayTimerForСhart = new Stopwatch();
        int googRequest = 0, errorRequest = 0;
        List<Request> requests = new List<Request>();
        ObservableCollection<String> textLog = new ObservableCollection<String>();
        private string textInProgress = "";
        private double progressBarValue;
        private double progressBarMaximunValue;
        private double progressBarMinimunValue;
        private int invalidateFlagPlot1;
        private int averageResponse;
        private int maxResponse;
        private int minResponse;
        private int totalErrors;
        private double bandwidth;
        private int totalRequests;
        private PlotModel modelP1;
        public IList<DataPoint> Points { get; private set; }
        public bool Run { get; private set; }
        public ICommand TestRunCommand { get; private set; }
        public ICommand TestCancelCommand { get; private set; }
        public Settings Model { get; private set; }
        bool progressFlag = true;
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        int timeForChart = 0;
        PieSeries seriesP1 = new PieSeries();
        Stopwatch operationTime = new Stopwatch();

        public TestWindowViewModel(Settings model, bool run)
        {
            this.Model = model;
            this.Run = run;
            this.Points = new List<DataPoint>
                              {
                                  new DataPoint(0, 0)
                              };


            this.modelP1 = new PlotModel
            {
                Title = "",
                LegendPlacement = LegendPlacement.Outside,
                LegendPosition = LegendPosition.BottomCenter,
                LegendOrientation = LegendOrientation.Horizontal,
                LegendBorderThickness = 0
            };

            seriesP1.InnerDiameter = 0;
            seriesP1.ExplodedDistance = 0.0;
            seriesP1.Stroke = OxyColors.White;
            seriesP1.StrokeThickness = 2.0;
            seriesP1.InsideLabelPosition = 0.8;
            seriesP1.AngleSpan = 360;
            seriesP1.StartAngle = 0;
            seriesP1.Slices.Add(new PieSlice("Successful request", 0) { IsExploded = true });
            seriesP1.Slices.Add(new PieSlice("Failed request", 0));
            Model1.Series.Add(seriesP1);
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            ProgressBarValue = 0;
            ProgressBarMinimunValue = 0;
            ProgressBarMaximumValue = 0;
            TextInProgress = "";
            this.TestRunCommand = new DelegateCommand(o => this.TestRun());
            this.TestCancelCommand = new DelegateCommand(o => this.TestCancel());
        }


        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            ProgressBarValue++;
            TextInProgress = String.Format("{0:####}%", ProgressBarValue * (100 * 1.0 / Model.Duration));
            timeForChart++;
        }
 
        public void TestCancel()
        {
            MessageBox.Show("Test Cancel");
            if (cts != null)
            {
                progressFlag = false;
                cts.Cancel();
            }

            dispatcherTimer.Stop();
        }
        public async void TestRun()
        {
            textLog.Clear();
            InvalidateFlagPlot1 = 0;
            progressFlag = true;
            dispatcherTimer.Stop();
            TextInProgress = string.Empty;
            ProgressBarValue = 0;
            ProgressBarMinimunValue = 0;
            ProgressBarMaximumValue = Model.Duration;
            timeForChart = 0;
            dispatcherTimer.Start();
            requests.Clear();
            googRequest = 0;
            errorRequest = 0;
            Points.Clear();
            TotalRequests = 0;
            Bandwidth = 0;
            TotalErrors = 0;
            MinResponse = 0;
            MaxResponse = 0;
            AverageResponse = 0;

            Model1.Title = "Real time pie chart!!!";

            cts = new CancellationTokenSource();

            Stopwatch delayTimer = new Stopwatch();
            int shiftTime = 0;
            try
            {
                cts.CancelAfter(ConvertToMilliseconds(Model.Duration));
                delayTimer.Start();
                while (delayTimer.Elapsed < TimeSpan.FromMilliseconds(ConvertToMilliseconds(Model.Duration)))
                {
                    delayTimerForСhart.Start();
                    await AccessTheWebAsync(cts.Token, shiftTime);
                }
            }
            catch (OperationCanceledException)
            {
                textLog.Add("The test has been completed.");
                textLog.Add("GoodRequesr = " + googRequest+ "   ErrorRequest = " + errorRequest);
                
                dispatcherTimer.Stop();
                if (progressFlag)
                {
                    ProgressBarValue = Model.Duration;
                    TextInProgress = String.Format("{0:####}% - Test done", 100);
                }
                else
                {
                    TextInProgress = "The test was aborted";
                }
                requests.Sort();
                TimeSpan sumResponseTime=new TimeSpan(0,0,0,0,0);
                double sumBytesSent = 0;
                foreach (var request in requests)
                {
                    sumResponseTime += request.ResponseTime;
                    sumBytesSent += request.ByteSent;
                }
                
                TotalRequests = errorRequest + googRequest;
                string[] splitMin = requests[0].ResponseTime.ToString().Split('.');
                string[] splitMax = requests[requests.Count-1].ResponseTime.ToString().Split('.');

                MinResponse = int.Parse(splitMin[1]);

                MaxResponse = int.Parse(splitMax[1]);

                TotalErrors = errorRequest;
                string[] averageResponseMs = sumResponseTime.ToString().Split('.');
                int averageResponseMsTotal = int.Parse(averageResponseMs[1]);
                AverageResponse = averageResponseMsTotal / requests.Count;
                Bandwidth = ((sumBytesSent/requests.Count) / AverageResponse)/1024*0.125;
            }
            catch (Exception)
            {
                dispatcherTimer.Stop();
            }
            cts = null;
        }

        private int ConvertToMilliseconds(int seconds)
        {
            return seconds * 1000;
        }

        async Task AccessTheWebAsync(CancellationToken ct, int shiftTime)
        {
            HttpClient client = new HttpClient();
            List<int> taskList = SetUpTaskList(Model.AmountOfThreads);
            IEnumerable<Task<int>> downloadTasksQuery =
               from url in taskList select ProcessURL(Model.URL, client, ct);
            List<Task<int>> downloadTasks = downloadTasksQuery.ToList();
            bool flagTimeOut = false;

            while (downloadTasks.Count > 0)
            {
                if (flagTimeOut)
                {
                    await Task.Delay(ConvertToMilliseconds(Model.AmountOfRequests), ct);
                }
                Task<int> firstFinishedTask = await Task.WhenAny(downloadTasks);
                downloadTasks.Remove(firstFinishedTask);
                operationTime.Restart();
                int length = await firstFinishedTask;
                TimeSpan time = operationTime.Elapsed;
                double y = length / 100000.0;

                if (length == 0)
                {
                    requests.Add(new Request(time, false, length));
                    errorRequest++;
                    AddPoint(Convert.ToDouble(timeForChart), y);
                    textLog.Add("Download error");
                    EditInfoInPieChart();
                }
                else
                {
                    requests.Add(new Request(time, true, length));
                    textLog.Add("Length of the download (Kbyte): " + length/1024 + "    time (ms): " + time);
                    googRequest++;
                    AddPoint(Convert.ToDouble(timeForChart), y);
                    EditInfoInPieChart();
                }
                flagTimeOut = true;
            }
        }
        private void EditInfoInPieChart()
        {
            seriesP1.Title = String.Format("Total request: {0}", googRequest + errorRequest);
            seriesP1.Slices.RemoveAt(0);
            seriesP1.Slices.Insert(0, new PieSlice("Successful request", googRequest) { IsExploded = true });
            seriesP1.Slices.RemoveAt(1);
            seriesP1.Slices.Insert(1, new PieSlice("Failed request", errorRequest) { IsExploded = true });
            Model1.InvalidatePlot(true);
        }
        private void AddPoint(double x, double y)
        {
            Points.Add(new DataPoint(x, y));
            InvalidateFlagPlot1++;
            InvalidateFlagPlot1++;
        }


        private List<int> SetUpTaskList(int countTask)
        {
            List<int> taskCount = new List<int>();
            for (int i = 0; i < countTask; i++)
            {
                taskCount.Add(i);
            }
            return taskCount;
        }

        async Task<int> ProcessURL(string url, HttpClient client, CancellationToken ct)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(url, ct);
                byte[] urlContents = await response.Content.ReadAsByteArrayAsync();
                return urlContents.Length;
            }
            catch
            {
                return 0;
            }
        }

        public int TotalRequests
        {
            get
            {
                return this.totalRequests;
            }
            set
            {
                if (value == this.totalRequests)
                    return;
                 totalRequests = value;
                 this.OnPropertyChanged();
            }
        }

        public double Bandwidth
        {
            get
            {
                return this.bandwidth;
            }
            set
            {
                if (value == this.bandwidth)
                    return;
                this.bandwidth = value;
                this.OnPropertyChanged();
            }
        }

        public int TotalErrors
        {
            get
            {
                return this.totalErrors;
            }
            set
            {
                if (value == this.totalErrors)
                    return;
                this.totalErrors = value;
                this.OnPropertyChanged();
            }
        }
        public int MinResponse
        {
            get
            {
                return this.minResponse;
            }
            set
            {
                if (value == this.minResponse)
                    return;
                this.minResponse = value;
                this.OnPropertyChanged();
            }
        }

        public int MaxResponse
        {
            get
            {
                return this.maxResponse;
            }
            set
            {
                if (value == this.maxResponse)
                    return;
                this.maxResponse = value;
                this.OnPropertyChanged();
            }
        }

        public int AverageResponse
        {
            get
            {
                return this.averageResponse;
            }
            set
            {
                if (value == this.averageResponse)
                    return;
                this.averageResponse = value;
                this.OnPropertyChanged();
            }
        }

        public string URL
        {
            get
            {
                return this.Model.URL;
            }
            set
            {
                if (value == this.Model.URL)
                    return;
                this.Model.URL = value;
                this.OnPropertyChanged();
            }
        }

        public int AmountOfThreads
        {
            get
            {
                return this.Model.AmountOfThreads;
            }
            set
            {
                if (value == this.Model.AmountOfThreads)
                    return;
                this.Model.AmountOfThreads = value;
                this.OnPropertyChanged();
            }
        }

        public int AmountOfRequests
        {
            get
            {
                return this.Model.AmountOfRequests;
            }
            set
            {
                if (value == this.Model.AmountOfRequests)
                    return;
                this.Model.AmountOfRequests = value;
                this.OnPropertyChanged();
            }
        }

        public int Duration
        {
            get
            {
                return this.Model.Duration;
            }
            set
            {
                if (value == this.Model.Duration)
                    return;
                this.Model.Duration = value;
                this.OnPropertyChanged();
            }
        }
        public ObservableCollection<String> TextLog
        {
            get
            {
                return textLog;
                // this.OnPropertyChanged("TextLog");
            }
        }
        public double ProgressBarValue
        {
            get
            {
                return progressBarValue;
            }
            set
            {
                if (value == this.progressBarValue)
                    return;
                progressBarValue = value;
                this.OnPropertyChanged();
            }
        }

        public double ProgressBarMaximumValue
        {
            get
            {
                return progressBarMaximunValue;
            }
            set
            {
                if (value == this.progressBarMaximunValue)
                    return;
                progressBarMaximunValue = value;
                this.OnPropertyChanged();
            }
        }

        public double ProgressBarMinimunValue
        {
            get
            {
                return progressBarMinimunValue;
            }
            set
            {
                if (value == this.progressBarMinimunValue)
                    return;
                progressBarMinimunValue = value;
                this.OnPropertyChanged();
            }
        }

        public string TextInProgress
        {
            get
            {
                return textInProgress;
            }
            set
            {
                if (value == this.textInProgress)
                    return;
                textInProgress = value;
                this.OnPropertyChanged();
            }
        }

        public int InvalidateFlagPlot1
        {
            get
            {
                return invalidateFlagPlot1;
            }
            set
            {
                if (value == this.invalidateFlagPlot1)
                    return;
                invalidateFlagPlot1 = value;
                this.OnPropertyChanged();
            }
        }

        public PlotModel Model1
        {
            get { return modelP1; }
            set {
                if (value == this.modelP1)
                    return;
                modelP1 = value; this.OnPropertyChanged();
            }
        }

    }
}
