﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication3.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
       // public event PropertyChangedEventHandler PropertyChanged;
        /*
                protected void OnPropertyChanged(string propertyName)
                {
                    this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
                }

                protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
                {
                    var handler = this.PropertyChanged;
                    if (handler != null)
                    {
                        handler(this, e);
                    }
                }
                */

        public event PropertyChangedEventHandler PropertyChanged;
        // Check the attribute in the following line :
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
