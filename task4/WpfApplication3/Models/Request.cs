﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication3.Models
{
    class Request : IComparable

    {
        public TimeSpan ResponseTime { get; set; }
        public bool Result { get; set; }
        public double ByteSent { get; set; }
        public Request (TimeSpan responseTime, bool result, double byteSent)
        {
            this.ResponseTime = responseTime;
            this.Result = result;
            this.ByteSent = byteSent;
        }

        public int CompareTo(Object o)
        {
            Request p = (Request)o;
            if (this.ResponseTime < p.ResponseTime)
                return -1;
            if (this.ResponseTime == p.ResponseTime)
                return 0;
            else
                return 1;
        }
        public override string ToString()
        {
            return (String.Format("{0};     {1};", ResponseTime, Result));
        }

    }
}
