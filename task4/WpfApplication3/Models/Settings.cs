﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication3.Models
{
    public class Settings
    {
     //   public string PageTitle { get; set; }
        public int AmountOfThreads { get; set; }
        public int AmountOfRequests { get; set; }
        public int Duration { get; set; }
        public string URL { get; set; }
    }
}
